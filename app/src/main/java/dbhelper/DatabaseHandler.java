package dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.EditText;

import com.arsoft.cocinapp.modelo.Ingrediente;
import com.arsoft.cocinapp.modelo.Paso;
import com.arsoft.cocinapp.modelo.Receta;
import com.arsoft.cocinapp.modelo.Recipiente;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton para acceso a bd
 * Esta clase deberia implementar los siguientes metodos :

/////////////////////////////Recetas
obtenerRecetas()
    Devuelve lista de recetas, para listar en pantalla de recetas.
id agregarReceta(Receta)
    agrega receta.

eliminarReceta(idReceta)
    Elimina una receta.

    agregarFotoGrandeReceta(idReceta, fotoGrande);
    agrega fotoGrande a una receta ya existente. fotoGrande tiene una url....


    /////////////////////////////Pasos
    obtenerPasos(idReceta)
    Devuelve una lista con los pasos de una receta.
            agregarPasos(lista Pasos)
    Agrega pasos (de una receta por primera vez)
    modificarPasos(idReceta,Lista Pasos)
    Borra todos los pasos existentes para esa receta y agrega los nuevos.

    /////////////////////////////Ingredientes
    obtenerIngredientes()
    Devuelve lista de ingredientes

    /////////////////////////////Recipientes
    obtenerRecipientes()
    Devuelve lista de recipientes
    EliminarRecipiente(id)
    Elimina recipiente.
    AgregarRecipiente(Recipiente)

    */


    public class DatabaseHandler extends SQLiteOpenHelper {

        // Logcat tag
        private static final String LOG = "DatabaseHandler";

    // Database Version
    private static final int DATABASE_VERSION = 130;

        // Database Name
        private static final String DATABASE_NAME = "cocinapp";

    private static final String INSERT_PASO =" INSERT INTO `PASO` (`id`, `idIngrediente`, `nombreIngrediente`, `unidad`, `medible`, `idReceta`, \n" +
            "`descripcion`,`cantidadIngrediente`,`foto`) VALUES \n" +
            "('1', '0', '','','0','1','Precalentar el horno a 175°c. Preparar y engrasar un molde de 22x32 cm(3.5L) con papel manteca','0','')," +
            "('2', '0', 'Chocolate Picado','','0','1','Coloque el chocolate picado en un bol grande y vierta  160 ml de agua hirviendo encima. Deje que se asiente 1 minuto y luego batir hasta derretir','0',''),"+
            "('3', '1', 'Agua','ml','1','1','Vierta  160 ml de agua hirviendo encima. Deje que se asiente 1 minuto y luego batir hasta derretir','160','')," +
            "('4', '10', 'Azucar','gr','1','1','Agregar batiendo 420 gr de azucar','420','')," +
            "('5', '0', 'Mayonesa','','0','1','Agregar una cucharada de mayonesa','0','')," +
            "('6', '0', 'Huevos','','0','1','Agregar 3 huevos, de a uno por vez','3','')," +
            "('7', '0', 'Esencia de vainilla','','0','1','Agregar esencia de vainilla','0','')," +
            "('8', '2', 'Harina','gr','1','1','Con una cuchara de madera agregar revolviendo 150 gr de Harina','150','')," +
            "('9', '6', 'Cacao en polvo','gr','1','1','Agregar 62 gr de Cacao en Polvo','62','')," +
            "('10', '0', 'Sal','','0','1','Agregar sal a gusto','0','')," +
            "('11', '0', 'Chips de chocolate','','0','1','Agregar chips de chocolate a gusto y revolver','0','')," +
            "('12', '0', '','','0','1','Pasar la mezcla al molde y hornee por 30 minutos. Enfriar a temperatura ambiente y Listo!','0','')," +
            "('13','0','','','0','7','Precalentar el horno a 190 grados','0','')," +
            "('14','0','Banana','','0','7','Pisar 2 bananas en un bol','0','')," +
            "('15','0','Manteca','gr','0','7','Agregar 105 gramos de manteca y mezclar','105','')," +
            "('16','0','Esencia de vainilla','','0','7','Agregar esencia de vainilla','0','')," +
            "('17','0','Huevo','','0','7','Agregar un Huevo','0','')," +
            "('18','2','Harina','gr','1','7','En un bol aparte, tamizar 200 gramos de Harina','200','')," +
            "('19','0','Polvo para hornear','','0','7','En el mismo bol agregar una cucharada de polvo parar hornear','0','')," +
            "('20','0','Sal','','0','7','Agregar Sal a gusto','0','')," +
            "('21','0','','','0','7','Junte ambas mezclas y revuelva hasta combinar','0','')," +
            "('22','0','Coco Rallado','ml','0','7','Agregar 125 ml de coco rallado','125','')," +
            "('23','0','','','0','7','Verter la mezcla con un cucharon en los pocillos de muffins','0','')," +
            "('24','0','','','0','7','Hornear por 20-25 minutos y Listo!','0','')," +
            "('25','0','','','0','6','Precalentar el horno a 175 grados y engrasar un molde de 23 x 13 cm (21)','0','')," +
            "('26','0','Manteca','gr','0','6','Agregar a un bol 75 gramos de manteca','75','')," +
            "('27','10','Azucar','gr','1','6','Agregar 210 gr de azúcar y mezclar hasta lograr una consistencia homogénea','210','')," +
            "('28','0','Banana','','0','6','Agregar 3 bananas pisadas','0','')," +
            "('29','0','Huevo','','0','6','Agregar un Huevo','0','')," +
            "('30','0','Esencia de vainilla','','0','6','Agregar esencia de vainilla','0','')," +
            "('31','2','Harina','gr','1','3','En un bol aparte agregar 110 gramos de harina común.','110','')," +
            "('32','2','Harina Integral','gr','1','6','Agregar 110 gramos de harina integral','110','')," +
            "('33','0','Bicarbonato','','0','6','Agregar una cucharada de bicarbonato','0','')," +
            "('34','0','Canela','','0','6','Agregar canela','0','')," +
            "('35','0','Sal','','0','6','Agregar Sal a gusto','0',''),"+
            "('36','0','','','0','6','Integrar las dos mezclas y combinar hasta lograr una mezcla homogénea','0','')," +
            "('37','0','Chips de Chocolate','','0','6','Sumar los chips de chocolate y pasar la mezcla al molde preparado','0','')," +
            "('38','0','','','0','6','Hornear por aproximadamente una hora y Listo!','0',''),"+
            "('39','0','','','0','4','Precalentar el horno a 175 grados y preparar dos placas con papel manteca (o engrasadas)','0','')," +
            "('40','0','Manteca','gr','0','4','Poner en un bol 115 gr de manteca','115','')," +
            "('41','11','Azúcar','gr','1','4','Agregar 150 gr de azúcar','150','')," +
            "('42','0','Huevo','','0','4','Añadir un huevo','0','')," +
            "('43','0','Esencia de vainilla','','0','4','Añadir escencia de vainilla','0','')," +
            "('44','2','Harina','gr','1','4','En un bol aparte agregar 260 gr de Harina','260','')," +
            "('45','7','Maicena','gr','1','4','Agregar 15 gramos de Maicena','15','')," +
            "('46','0','Bicarbonato','','0','4','Agregar una cucharada Bicarbonato','0','')," +
            "('47','0','Sal','','0','4','Agregar Sal a gusto','0','')," +
            "('48','0','','','0','4','Integrar las dos mezclas y revolver hasta homogeneizar','0','')," +
            "('49','0','Chips de chocolate','','0','4','Agregar los chips de chocolate y revolver','0','')," +
            "('50','0','','','0','4','Arme las cookies sobre las placas dejando 2 cm de distancia entre cada galleta','0','')," +
            "('51','0','','','0','4','Hornear durante 10 minutos hasta que las cookies tengan un color dorado intenso y Listo!','0','')," +
            "('52','0','','','0','5','Precalentar el horno a 175 grados','0','')," +
            "('53','11','Azúcar','gr','1','5','Agregar 65 gr de azúcar','65','')," +
            "('54','0','Manzana','','0','5','Agregar 4 manzanas peladas y cortadas en rodajas','0','')," +
            "('55','0','Canela','','0','5','Agregar media cucharada de canela','0','')," +
            "('56','0','','','0','5','Esparcir la mezcla sobre un molde para horno','0','')," +
            "('57','0','Manteca','gr','0','5','Poner en otro bol 115 gr de manteca','115','')," +
            "('58','10','Azúcar','gr','1','5','Agregar 150 gr de azucar','150','')," +
            "('59','0','Huevo','','0','5','Añadir un huevo','0','')," +
            "('60','0','Esencia de vainilla','','0','5','Añadir Esencia de vainilla','0','')," +
            "('61','2','Harina','gr','1','5','En un bol aparte agregar 260 gr de Harina','260','')," +
            "('62','7','Maicena','gr','1','5','Agregar 15 gramos de Maicena','15','')," +
            "('63','0','Bicarbonato','','0','5','Agregar una cucharada Bicarbonato','0','')," +
            "('64','0','Sal','','0','5','Agregar Sal a gusto','0','')," +
            "('65','0','','','0','5','Integrar las dos mezclas y revolver hasta homogeneizar','0','')," +
            "('66','0','Canela Molida','','0','5','Agregar media cucharada de canela molida.','0','')," +
            "('67','0','','','0','5','Desmigaje la masa sobre las manzanas.','0','')," +
            "('68','0','','','0','5','Hornear el crumble de 35 a 40 minutos hasta que la parte superior se dore y se cocine bien y Listo!','0',''),"+
            "('69','10','Azúcar','gr','1','3','Agregar 100 gramos de azúcar','100',''),"+
            "('70','9','Crema','ml','1','3','Colocar 180 ml de crema en un bowl','180',''),"+
            "('71','0','Esencia de vainilla','','0','3','Agregar unas gotas de esencia de vainilla','0',''),"+
            "('72','0','','','0','3','Batir hasta lograr una consistencia sólida y Listo!','0','')," +
            "('73','0','','','0','2','Precalentar el horno a temperatura media','0','')," +
            "('74','0','Huevos','','0','2','Poner en un bowl 3 huevos','0','')," +
            "('75','10','Azúcar','gr','1','2','Agregar al bowl 180 gr de azúcar','180','')," +
            "('76','2','Harina','gr','1','2','Agregar al bowl 120 gr de harina','120','')," +
            "('77','7','Maizena','gr','1','2','Agregar al bowl 60 gr de maizena','60','')," +
            "('78','0','Esencia de vainilla','','0','2','Agregar unas gotas de esencia de vainilla y batir hasta lograr una mezcla homogenea','0','')," +
            "('79','0','','','0','2','Volcar en un molde enmantecado','0','')," +
            "('80','0','','','0','2','Hornear a 170 ° durante 45 a 55 minutos y Listo!','0','');";


//    private static final String INSERT_RECETA = "INSERT INTO `RECETA` (`id`, `nombre`, `descripcion`, `fotoChica`,fotoGrande, ingredientes) VALUES \n" +
//           "('1', 'Brownie de chocolate', 'La clave para estos sustanciosos y chocolatosos brownies es... ¡la mayonesa! Este inesperado ingrediente toma el lugar de la manteca (mantequilla) y ayuda a mantener la humedad. Esta es la receta de brownies que rinde más, y además, se freezan muy bien.','fotochica','@drawable/brownie','Chocolate sin azucar, Agua, Azúcar, Mayonesa, Huevos, Extracto de vainilla, Harina 0000, Cacao en polvo, Sal, Chips de chocolate'),"+
//           "('2','Muffins de Banana y Coco','Muffin de Banana y Coco para hacer la tarde mas llevadera','fotochica','@drawable/muffinbyc','Banana, Azúcar, Huevo, Esencia de vainilla, Harina, Manteca, Polvo para hornear, Sal, Coco rallado')," +
//           "('3','Budín de Banana y Chocolate','Un buen acompañante del mate.','fotochica','@drawable/budinbyc','Manteca, Azúcar, Banana, Huevo, Esencia de vainilla, Harina, Harina integral, Bicarbonato de sodio, Canela molida, Sal, Chips de chocolate')," +
//           "('4','Cookies de Chips de Chocolate','No hay nada más casero que esto, una rica masa básica de cookies y un montón de chips de chocolate.','fotochica','@drawable/cookies','Manteca, Azúcar, Huevo, Esencia de vainilla, Harina, Bicarbonato de sodio, Sal, Maicena')," +
//           "('5','Crumble de Manzana','Delicioso por donde se lo mire','fotochica','@drawable/crumble','Manteca, Azúcar, Huevo, Esencia de vainilla, Harina, Bicarbonato de sodio, Sal, Maicena, Canela molida, Manzana')," +
//            "('6','Crema chantilly','El relleno perfecto','fotochica','@drawable/cremachantilly','Crema, Azúcar, Esencia de vainilla'),"+
//            "('7','Bizcochuelo','Simple y Delicioso','fotochica','@drawable/bizcochuelo','Huevos, Azúcar, Harina, Maizena, Esencia de vainilla');";

    private static final String INSERT_RECETA = "INSERT INTO `RECETA` (`id`, `nombre`, `descripcion`, `fotoChica`,fotoGrande, ingredientes) VALUES \n" +
            "('1', 'Brownie de chocolate', 'La clave para estos sustanciosos y chocolatosos brownies es... ¡la mayonesa! Este inesperado ingrediente toma el lugar de la manteca (mantequilla) y ayuda a mantener la humedad. Esta es la receta de brownies que rinde más, y además, se freezan muy bien.','fotochica','@drawable/brownie','Chocolate sin azucar, Agua, Azúcar, Mayonesa, Huevos, Extracto de vainilla, Harina 0000, Cacao en polvo, Sal, Chips de chocolate'),"+
            "('2','Bizcochuelo','Simple y Delicioso','fotochica','@drawable/bizcochuelo','Huevos, Azúcar, Harina, Maizena, Esencia de vainilla')," +
            "('3','Crema chantilly','El relleno perfecto','fotochica','@drawable/cremachantilly','Crema, Azúcar, Esencia de vainilla')," +
            "('4','Cookies de Chips de Chocolate','No hay nada más casero que esto, una rica masa básica de cookies y un montón de chips de chocolate.','fotochica','@drawable/cookies','Manteca, Azúcar, Huevo, Esencia de vainilla, Harina, Bicarbonato de sodio, Sal, Maicena')," +
            "('5','Crumble de Manzana','Delicioso por donde se lo mire','fotochica','@drawable/crumble','Manteca, Azúcar, Huevo, Esencia de vainilla, Harina, Bicarbonato de sodio, Sal, Maicena, Canela molida, Manzana')," +
            "('6','Budín de Banana y Chocolate','Un buen acompañante del mate.','fotochica','@drawable/budinbyc','Manteca, Azúcar, Banana, Huevo, Esencia de vainilla, Harina, Harina integral, Bicarbonato de sodio, Canela molida, Sal, Chips de chocolate'),"+
            "('7','Muffins de Banana y Coco','Muffin de Banana y Coco para hacer la tarde mas llevadera','fotochica','@drawable/muffinbyc','Banana, Azúcar, Huevo, Esencia de vainilla, Harina, Manteca, Polvo para hornear, Sal, Coco rallado');";

    private static final String INSERT_INGREDIENTE = "INSERT INTO `INGREDIENTE` (`id`, `nombre`, `unidad`, `delta`) VALUES\n" +
            "(1,\"Agua\",\"ml\",1),\n" +
            "(2,\"Harina 0000\",\"gr\",0.55),\n" +
            "(3,\"Leche Descremada\",\"ml\",1.07),\n" +
            "(4,\"Café Molido\",\"gr\",0.45),\n" +
            "(5,\"Coco Rallado\",\"gr\",0.48),\n" +
            "(6,\"Cacao en Polvo\",\"gr\",0.67),\n" +
            "(7,\"Maicena\",\"gr\",0.64),\n" +
            "(8,\"Azúcar Negra\",\"gr\",0.8),\n" +
            "(9,\"Crema\",\"ml\",1.03),\n" +
            "(10,\"Azucar\",\"gr\",0.9);";

    // Table Create Statements

    // Tag table create statement

    private static final String CREATE_TABLE_INGREDIENTE = "\n" +
            "Create table ingrediente (\n" +
            "id INTEGER PRIMARY KEY,\n" +
            "nombre text,\n" +
            "unidad text,\n" +
            "delta float  );";

    private static final String CREATE_TABLE_PASO = "create table paso (\n" +
            "id INTEGER PRIMARY KEY,\n" +
            "idIngrediente int, \n" +
            "nombreIngrediente text,\n" +
            "unidad text,\n" +
            "medible int,\n" +
            "idReceta int,\n" +
            "descripcion text,\n" +
            "cantidadIngrediente int,\n" +
            "foto text,\n"+
            "nombreOtroIngrediente text)";


    private static final String CREATE_TABLE_RECETA = "create table receta (\n" +
            "id INTEGER PRIMARY KEY,\n" +
            "nombre text,\n" +
            "descripcion text,\n" +
            "fotoChica text,\n" +
            "fotoGrande text,\n" +
            "ingredientes text)";

    private static final String CREATE_TABLE_RECIPIENTE = "create table recipiente (\n" +
            "id INTEGER PRIMARY KEY,\n" +
            "volumen text,\n" +
            "nombre text,\n" +
            "foto text,\n" +
            "altura REAL,\n" +
            "ancho REAL,\n" +
            "radioChico REAL,\n" +
            "radioGrande REAL)";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_INGREDIENTE);
        db.execSQL(CREATE_TABLE_PASO);
        db.execSQL(CREATE_TABLE_RECETA);
        db.execSQL(CREATE_TABLE_RECIPIENTE);
        db.execSQL(INSERT_INGREDIENTE);
        db.execSQL(INSERT_RECETA);
        db.execSQL(INSERT_PASO);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS ingrediente");
        db.execSQL("DROP TABLE IF EXISTS paso");
        db.execSQL("DROP TABLE IF EXISTS receta");
        db.execSQL("DROP TABLE IF EXISTS recipiente");

        // create new tables
        onCreate(db);
    }


    public DatabaseHandler inicializar() { //Este metodo hace la carga inicial

        List<Receta> recetas;
        recetas = obtenerRecetas(); //Hago esto para ver si ya habian datos...

        if(recetas.size()!=0){ //Si entro aca es porque la carga inicial ya estaba hecha
            System.out.println("La BD ya estaba inicializada...");
            return this;
        }


        System.out.println("Se ejecuta el metodo inicializar de la BD");
        //Si llego aca es porque la bd esta vacia y debo hacer carga inicial
        recetas = new ArrayList<>();
        List<Paso> listaPasos = new ArrayList<Paso>();
        //
             List<Paso> pasosObtenidos = new ArrayList<Paso>();


        System.out.println("SE CARGARON LOS PASOS Y EL TAMAÑO ES:"+ pasosObtenidos.size());
        for (int i=0;i<pasosObtenidos.size();i++) {
            System.out.println("ID P:" + listaPasos.get(i).getId()+ "ID R:"+ listaPasos.get(i).getIdReceta()+"DES:"+listaPasos.get(i).getDescripcion());
        }



        return this;
    }


    /**
     * Obtener todas recetas de la db
     * @return Lista Receta
     */
    public List<Receta> obtenerRecetas() {
        List<Receta> recetas = new ArrayList<Receta>();
        String selectQuery = "SELECT  * FROM receta";

        Log.d(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Receta cn[] = new Receta[150];
        // looping through all rows and adding to list
        int i = 0;
        if (c.moveToFirst()) {
            do {
                cn[i] = new Receta();
                //Cargo este elemento en cn
                cn[i].setId(Integer.parseInt(c.getString(0)));
                System.out.println("EN DBHANDLER - Saque de la db la receta:" + c.getString(1));
                cn[i].setNombre(c.getString(1));
                cn[i].setDescripcion(c.getString(2));
                cn[i].setFotoChica(c.getString(3));
                cn[i].setFotoGrande(c.getString(4));
                cn[i].setIngredientes(c.getString(5));

                // La agrego a la lista que voy a devolver
                recetas.add(cn[i]);
                i++;
            } while (c.moveToNext());
        }

        db.close();

        return recetas;
    }

    /**
     * Agrega la receta
     * @param receta
     * @return ok/error
     */
    public int agregarReceta(Receta receta){
        int id;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //El id no lo ingresamos porque se calcula solo
        values.put("nombre", receta.getNombre()); // Contact Name
        values.put("descripcion", receta.getDescripcion()); // Contact Name
        values.put("fotoChica", receta.getFotoChica()); // Contact Name
        values.put("fotoGrande", receta.getFotoGrande()); // Contact Name
        values.put("ingredientes", receta.getIngredientes()); // Contact Name

        // Inserting Row
        id=(int)db.insert("receta", null, values); //Agrega la receta y me devuelve el id
        db.close(); // Closing database connection


        return id;
    }


    /**
     * Borra la receta segun id
     * @param idReceta
     */
    public void eliminarReceta(int idReceta){

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from receta where id ="+idReceta);
        System.out.println("Elimine la receta: "+idReceta);
        db.close();

    }

    /**
     * Borra los pasos de la receta segun el id de receta
     * @param idReceta
     */
    public void eliminarPasosReceta(int idReceta){

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from paso where idReceta ="+idReceta);
        System.out.println("Elimine los pasos de la receta: "+idReceta);
        db.close();

    }

    /**
     * Agrega la foto grande a la receta segun ID
     * @param idReceta
     * @param fotoGrande
     */

    public void agregarFotoGrandeReceta(int idReceta, String fotoGrande){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues data=new ContentValues();
        data.put("fotoGrande",fotoGrande);
        db.update("receta", data, "id=" + idReceta, null);
        System.out.println("agregue fotoGrande a receta: "+idReceta +" - fotoGrande="+fotoGrande);
        db.close();
    }

    /**
     * Obtiene pasos de la receta segun ID.
     * @param idReceta
     * @return
     */
    public List<Paso> obtenerPasos(int idReceta){
        List<Paso> pasos = new ArrayList<Paso>();
        String selectQuery = "SELECT  * FROM paso where idReceta ="+idReceta;

        Log.d(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Paso pasoObtenido;
        // looping through all rows and adding to list
        int i = 0;
        if (c.moveToFirst()) {
            do {
                pasoObtenido = new Paso();
                //Cargo este elemento en cn
                pasoObtenido.setId(Integer.parseInt(c.getString(0)));
                System.out.println("EN DBHANDLER - Saque de la db el paso:" + c.getString(1));
                pasoObtenido.setIdIngrediente(Integer.parseInt(c.getString(1)));
                pasoObtenido.setNombreIngrediente(c.getString(2));
                pasoObtenido.setUnidad(c.getString(3));
                pasoObtenido.setMedible(Integer.parseInt(c.getString(4)));
                pasoObtenido.setIdReceta(Integer.parseInt(c.getString(5)));
                pasoObtenido.setDescripcion(c.getString(6));
                pasoObtenido.setCantidadIngrediente(Integer.parseInt(c.getString(7)));
                pasoObtenido.setFoto(c.getString(8));
                pasoObtenido.setNombreOtroIngrediente(c.getString(9));

                System.out.println("En cn tengo el paso: " + pasoObtenido.getDescripcion());
                pasos.add(pasoObtenido);
                i++;
            } while (c.moveToNext());
        }

        db.close();
        return pasos;

    }

    /**
     * Modifica la descripcion de la receta segun ID
     * Se usa cuando queremos agregar lista de ingredientes
     * @param descripcion
     * @param idReceta
     */
    public void modificarDescripcionReceta(String descripcion,int idReceta) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(String.format("UPDATE receta SET descripcion='%s' WHERE id=%d",descripcion,idReceta));
        System.out.println("Modifique la descripcion de la receta: "+idReceta);
        db.close();
    }
    public void modificarFotoRecipiente(String foto,int idRecipiente) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(String.format("UPDATE recipiente SET foto='%s' WHERE id=%d",foto,idRecipiente));
        Log.i("FOTO","Modifique la foto del recipiente a: "+idRecipiente);
        db.close();
    }

    /**
     * Modifica los ingrdientes de la receta segun ID
     * Se usa cuando queremos agregar lista de ingredientes
     * @param ingredientes
     * @param idReceta
     */
    public void modificarIngredientesReceta(String ingredientes,int idReceta) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(String.format("UPDATE receta SET ingredientes='%s' WHERE id=%d",ingredientes,idReceta));
        System.out.println("Modifique los ingredientes de la receta: "+idReceta);
        db.close();
    }


    /**
     * Agrega pasos a la base de datos,
     * contiene en la lista los id de receta para relacionarse
     * @param pasos
     */
    public void agregarPasos( List<Paso> pasos ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values;

        for (int i = 0; i < pasos.size(); i++) {
            Paso o =  pasos.get(i);

            values = new ContentValues();
            //El id no lo ingresamos porque se calcula solo
            values.put("idIngrediente", o.getIdIngrediente());
            values.put("idReceta", o.getIdReceta()); //
            values.put("nombreIngrediente",o.getNombreIngrediente());
            values.put("unidad",o.getUnidad());
            values.put("medible",o.getMedible());
            values.put("descripcion", o.getDescripcion());
            values.put("cantidadIngrediente", o.getCantidadIngrediente());
            values.put("foto", o.getFoto());
            values.put("nombreOtroIngrediente", o.getNombreOtroIngrediente());


            db.insert("paso", null, values);
        }

        db.close();

    }

    /**
     * Modificar paso, no esta correcto hay que usar UPDATE
     * @param paso
     */
    public void modificarPasos(Paso paso ){

        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(String.format("UPDATE paso SET idIngrediente=%d,nombreIngrediente='%s', unidad='%s',medible=%d,descripcion='%s',cantidadIngrediente=%d, nombreOtroIngrediente='%s' where id=%d",paso.getIdIngrediente(),paso.getNombreIngrediente(),paso.getUnidad(),paso.getMedible(),paso.getDescripcion(),paso.getCantidadIngrediente(),paso.getNombreOtroIngrediente(),paso.getId()));

        db.close();
    }


    /**
     * Obtener todos los ingredientes
     * @return
     */
    public List<Ingrediente> obtenerIngredientes(){

        List<Ingrediente> ingredientes = new ArrayList<>();
        String selectQuery = "SELECT  * FROM ingrediente";

        Log.d(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Ingrediente cn;
        // looping through all rows and adding to list
        int i = 0;
        if (c.moveToFirst()) {
            do {
                cn = new Ingrediente();
                //Cargo este elemento en cn
                cn.setId(Integer.parseInt(c.getString(0)));
                System.out.println("EN DBHANDLER - Saque de la db el ingrediente:" + c.getString(1));
                cn.setNombre(c.getString(1));
                cn.setUnidad(c.getString(2));
                cn.setDelta(Float.parseFloat(c.getString(3)));

                System.out.println("En cn tengo el ingred:" + cn.getNombre());
                ingredientes.add(cn);
                i++;
            } while (c.moveToNext());
        }

        db.close();
        return ingredientes;

    }

    /**
     * Agrea ingredientes a la base de datos
     * @param ingrediente
     */
    public void agregarIngrediente(Ingrediente ingrediente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id", ingrediente.getId()); //
        values.put("nombre", ingrediente.getNombre()); //
        values.put("unidad", ingrediente.getUnidad()); //
        values.put("delta", ingrediente.getDelta()); //

        // Inserting Row
        db.insert("ingrediente", null, values);
        db.close(); // Closing database connection
        System.out.println("Se agrego el Ingrediente " + ingrediente.getNombre());
    }

    /**
     * Obtiene lista de recipientes
     * NO IMPLEMENTADO TODAVIA
     * @return
     */
    public List<Recipiente> obtenerRecipientes(){

        List<Recipiente> recipientes = new ArrayList<Recipiente>();
        String selectQuery = "SELECT  * FROM recipiente";

        Log.d(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Recipiente cn;
        // looping through all rows and adding to list
        int i = 0;
        if (c.moveToFirst()) {
            do {
                cn = new Recipiente();
                //Cargo este elemento en cn
                cn.setId(Integer.parseInt(c.getString(0)));
                System.out.println("EN DBHANDLER - Saque de la db el recipiente:" + c.getString(1));
                cn.setVolumen(c.getDouble(1));
                cn.setNombre(c.getString(2));
                cn.setFoto(c.getString(3));
                cn.setAltura(c.getDouble(4));
                cn.setAncho(c.getDouble(5));
                cn.setRadioChico(c.getDouble(6));
                cn.setRadioGrande(c.getDouble(7));

                System.out.println("En cn tengo el recip:" + cn.toString());
                // La agrego a la lista que voy a devolver
                recipientes.add(cn);
                i++;
            } while (c.moveToNext());
        }

        db.close();
        return recipientes;


    }

    /**
     * Elimina recipiente de la BD segun ID
     * @param idRecipiente
     */
    public void eliminarRecipiente(int idRecipiente){

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from recipiente where id ="+idRecipiente);
        System.out.println("Elimine el recipiente: "+idRecipiente);
        db.close();

    }



    /**
     * Agrega recipiente a la base de datos
     * @param recipiente
     */
    public void agregarRecipiente(Recipiente recipiente) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("volumen", recipiente.getVolumen());
        values.put("nombre", recipiente.getNombre());
        values.put("foto", recipiente.getFoto());
        values.put("altura", recipiente.getAltura());
        values.put("ancho", recipiente.getAncho());
        values.put("radioChico", recipiente.getRadioChico());
        values.put("radioGrande", recipiente.getRadioGrande());

        db.insert("recipiente", null, values);
        db.close(); // Closing database connection
        System.out.println("Se agrego el recipiente " + recipiente.getNombre());
    }

    public void modificarRecetaPrincipal(EditText descripcionReceta, EditText ingredientesReceta, int postion) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(String.format("UPDATE receta SET ingredientes='%s', descripcion='%s' WHERE id=%d",ingredientesReceta.getText(),descripcionReceta.getText(),postion+1));
        System.out.println("Modifique los ingredientes de la receta: "+postion);
        db.close();
    }
}