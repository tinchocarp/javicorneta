package com.arsoft.cocinapp.opencv;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.arsoft.cocinapp.events.ConfirmGuardarRecipienteHandler;
import com.arsoft.cocinapp.events.GuardarRecipienteHandler;
import com.arsoft.cocinapp.modelo.Cosito;
import com.arsoft.cocinapp.modelo.Recipiente;
import com.arsoft.cocinapp.utilitarios.Calculo;
import com.arsoft.cocinapp.R;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ReconocimientoActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static String TAG = "ModuloReconocimiento";
//    final double FACTOR_ALTURA=0.915;
final double FACTOR_ALTURA=0.915;
    final double FACTOR_RADIO_CHICO=1.05;

    private JavaCameraView2 jvc2; //Para encender el flash.
    private boolean flash; //Flash encendido o no
    private Mat editedFrame;
    private Mat editedFrame2;
    private Mat rgba;
    private Mat element;
    private Mat anterior;
    private List<MatOfPoint> contours;
    private List<MatOfPoint> matchedContours;
    private List<MatOfPoint> puntos;
    private MatOfPoint2f approxCurve;
    private Rect rect;
    private MatOfPoint2f contour2f;
    private MatOfPoint points;
    private ArrayList<Rect> rects;
    private Rect primero;
    private Rect segundo;
    private double heightLinea=0;
    private MatOfPoint puntosSegundo;
    private Point vectorPuntos[];

    private ImageButton btnGuardarReceta;

    private LinkedList<Recipiente> recipientes;
    private Recipiente recipiente;

    private CameraBridgeViewBase mOpenCvCameraView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {mOpenCvCameraView.enableView();
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called esdfgate");
        flash = false;

        if (Cosito.getInstance().getTamanio() == 0){
            Toast.makeText(this,"Falta indicar el tamaño de la referencia", Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(this, ConfiguracionActivity.class);
//            startActivity(intent);
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconocimiento);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Agregar Recipiente");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.HelloOpenCvView);
        puntos = new ArrayList<MatOfPoint>();
        mOpenCvCameraView.setMaxFrameSize(640,480);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        jvc2 = (JavaCameraView2) findViewById(R.id.HelloOpenCvView);

        btnGuardarReceta = (ImageButton) findViewById(R.id.fab);



        new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(btnGuardarReceta))
                .setContentTitle("¿Cómo medir?")
                .setContentText("Apunta con tu cámara al recipiente que vas a usar para medir. Debes poner el objeto Referencia a la izquierda del recipiente y el fondo debe contrastar con los objetos.")
                .singleShot(43)
                .build();


    }//Llave de fin onCreate

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        rgba = new Mat();
        editedFrame = new Mat();
        editedFrame2 = new Mat();
        element = new Mat();
        anterior = new Mat();
        contours = new ArrayList<>();
        matchedContours = new ArrayList<>();
        approxCurve = new MatOfPoint2f();
        rects = new ArrayList<>();
        if (!flash) {
            Log.e(TAG, "Entre al if q enciende flash");
            flash = true;
            jvc2.setupCameraFlashLightOn();
        }

        recipiente = new Recipiente();
        recipientes = new LinkedList<Recipiente>();
    }

    public void onCameraViewStopped() {
        rgba.release();
        editedFrame.release();
        editedFrame2.release();
        element.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        rgba = inputFrame.rgba();
        contours.clear();
        matchedContours.clear();
        rects.clear();
        puntos.clear();

        aplicarFiltros();

        Imgproc.findContours(editedFrame,contours, new Mat(),Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);

        for (MatOfPoint contour : contours){
            if (Imgproc.contourArea(contour)>1000) //Descarto contornos chicos.
                matchedContours.add(contour);
        }

        if (matchedContours.size() != 2) { //Si hay menos dos de dos objetos, sigo capturando.
            return rgba;
        }

        int izquierda = 0;
        //Uso esto como flag para ver cual contorno esta mas a la izq. Si es 0 es el primero y si es 1 es el segundo

        for (int i = 0; i < matchedContours.size(); i++) {

            contour2f = new MatOfPoint2f(matchedContours.get(i).toArray());
            double approxDistance = Imgproc.arcLength(contour2f, true)*0.01;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
            points = new MatOfPoint(approxCurve.toArray());

            rect = Imgproc.boundingRect(points);
            rects.add(rect);
            puntos.add(points);

            if (i == 0) {
                izquierda = rect.x;
            } else {
                if (izquierda < rect.x) {
                    izquierda = 0;
                } else {
                    izquierda = 1;
                }
            }

            Imgproc.rectangle(rgba, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar( 0, 0, 255 ),0,20, 0);
        }


        if (rects.size() < 2) {
            return rgba; //Regresa la imagen, no hay dos rectangulos.
        }
        if (izquierda == 0) { //Acomodo segun orden izq a derecha
            segundo = rects.get(1);
            puntosSegundo = puntos.get(1);
        } else {
            segundo = rects.get(0);
            puntosSegundo = puntos.get(0);
        }

        //Inicio de calculo para reconcer tamaÃ±o

        primero = rects.get(izquierda); //Este es el primero ( el contorno de mas a la izq. )

        double distanciaeu = distanciaEuclideana(new Point(primero.x, primero.y), new Point(primero.x + primero.width, primero.y));
        double pixelsPerMetric = distanciaeu / Cosito.getInstance().getTamanio(); //tamaÃ±ofijo es en centimetros el tamaÃ±o del objeto que ya conozco
        Imgproc.putText(rgba, String.format("T[%.2f]", Cosito.getInstance().getTamanio()), new Point(0,150), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);


        //Calculo del segundo contorno detectado
        distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y), new Point(segundo.x + segundo.width, segundo.y));
        double ancho = distanciaeu / pixelsPerMetric;

        Imgproc.putText(rgba,String.format("W[%.2f]", ancho) , new Point(0,200), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);

        distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y), new Point(segundo.x, segundo.y + segundo.height));
        double alto = distanciaeu / pixelsPerMetric;
        alto = alto * FACTOR_ALTURA;


        Imgproc.putText(rgba, String.format("H[%.2f]", alto), new Point(0,250), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);

        //Busco el radio chico

        ////Voy a meter codigo para dibujar circulos en cada punto del contorno
        vectorPuntos = puntosSegundo.toArray();

        double menor=0;
        int flag = 0;
        Point puntoInferiorIzquierdo = new Point();
        Point puntoInferiorDerecho = new Point();

        for (int j = 0; j < vectorPuntos.length; j++) {//Busco el punto con la menor distancia al punto izq inferior del rectangulo
            //Por cada punto quiero :
            /* //Dibujarlos
            Imgproc.circle(rgba, vectorPuntos[j], 6, new Scalar(255, 0, 0));
            Imgproc.putText(rgba,"("+j+")",vectorPuntos[j], Core.FONT_HERSHEY_COMPLEX_SMALL,2,new Scalar(255,0,0),4);
            */
            distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y+segundo.height), vectorPuntos[j]);
            if(flag==0){
                flag=1;
                menor=distanciaeu;
                puntoInferiorIzquierdo=vectorPuntos[j];
            }
            if(distanciaeu<menor){
                menor=distanciaeu;
                puntoInferiorIzquierdo=vectorPuntos[j];
            }


        }//Llave del for que recorre los puntos


        //Busco el punto inferior derecho
        flag=0;
        for (int j = 0; j < vectorPuntos.length; j++) {//Busco el punto con la menor distancia al punto der inferior del rectangulo
            //Por cada punto quiero :
            /* //Dibujarlos
            Imgproc.circle(rgba, vectorPuntos[j], 6, new Scalar(255, 0, 0));
            Imgproc.putText(rgba,"("+j+")",vectorPuntos[j], Core.FONT_HERSHEY_COMPLEX_SMALL,2,new Scalar(255,0,0),4);
            */
            distanciaeu = distanciaEuclideana(new Point(segundo.x+segundo.width,segundo.y+segundo.height), vectorPuntos[j]);
            if(flag==0){
                flag=1;
                menor=distanciaeu;
                puntoInferiorDerecho=vectorPuntos[j];
            }
            if(distanciaeu<menor){
                menor=distanciaeu;
                puntoInferiorDerecho=vectorPuntos[j];
            }


        }//Llave del for que recorre los puntos




        //Dibujo el puntoInferiorIzquierdo a ver si lo reconoci bien
        Imgproc.circle(rgba, puntoInferiorIzquierdo, 6, new Scalar(255, 0, 0));
        //Ahora que tengo el punto inferior izquierdo puedo calcular el radio chico
        distanciaeu = distanciaEuclideana(new Point(segundo.x, puntoInferiorIzquierdo.y), puntoInferiorIzquierdo);
        double distanciaAlIzquierdo = distanciaeu/pixelsPerMetric;
        //Imgproc.putText(rgba, String.format("RC[%.2f]", radioChico), new Point(0,200), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);

        //Dibujo el puntoInferiorDerecho a ver si lo reconoci bien
        Imgproc.circle(rgba, puntoInferiorDerecho, 6, new Scalar(255, 0, 0));

        double radioChico;
        //Ahora que tengo el punto inferior derecho Puedo calcular el radio chico
        distanciaeu = distanciaEuclideana(new Point(puntoInferiorDerecho.x, puntoInferiorDerecho.y), new Point(puntoInferiorIzquierdo.x,puntoInferiorIzquierdo.y));
        radioChico = distanciaeu/pixelsPerMetric;
        radioChico=radioChico*FACTOR_RADIO_CHICO;

        Imgproc.putText(rgba, String.format("RC[%.2f]", radioChico), new Point(0,300), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);


        //Fin de calculos para reconocer el tamaÃ±o

        Log.w(TAG,"Alto: " + alto  + " RChico: " + radioChico/2 + "Grande: " +ancho/2);
        double volumen = Calculo.obtenerMedidas(alto,radioChico/2,ancho/2);
        Log.w(TAG,"Volumen total: " + volumen);

        recipiente.setAltura(alto );
        recipiente.setAncho(ancho);
        recipiente.setRadioGrande(ancho     / 2);
        recipiente.setRadioChico(radioChico / 2);
        recipiente.setVolumen(volumen);
        recipiente.setNombre("Recipiente");

        recipientes.addFirst(recipiente);
        if (recipientes.size() > 10){
            recipientes.removeLast();
        }

        btnGuardarReceta.setOnClickListener(new ConfirmGuardarRecipienteHandler(new GuardarRecipienteHandler(this, recipientes, rgba)));
        anterior = rgba;
        return rgba;
    }

    private void aplicarFiltros(){
        Imgproc.cvtColor(rgba, editedFrame,Imgproc.COLOR_RGBA2GRAY);
        Imgproc.GaussianBlur(editedFrame, editedFrame,new Size(15,15),0);
        Imgproc.Canny(editedFrame, editedFrame,500, 1500, 5, false);

        int dilation_size = 5;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*dilation_size + 1,2*dilation_size+1 ));
        Imgproc.dilate(editedFrame, editedFrame,element);

        int erotion_size = 5;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*erotion_size+1,2*erotion_size+1));
        Imgproc.erode(editedFrame, editedFrame,element);

//        if (true) return editedFrame;
    }

    private double distanciaEuclideana(Point p1, Point p2) { //Funcion simple que calcula distancia entre puntos
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

