package com.arsoft.cocinapp.opencv;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.arsoft.cocinapp.actividades.ConfiguracionActivity;
import com.arsoft.cocinapp.modelo.RecipienteSeleccionado;
import com.arsoft.cocinapp.utilitarios.Calculo;
import com.arsoft.cocinapp.R;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class MarcarNivelActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final double FACTOR_LINEA_FINAL = 1.2;


    private static String TAG = "ModuloReconocimiento";
    private JavaCameraView2 jvc2; //Para encender el flash.
    private boolean flash; //Flash encendido o no
    private SeekBar seekBar;
    private int progreso = 0;
    private Mat editedFrame;
    private Mat rgba;
    private Mat element;
    private List<MatOfPoint> contours;
    private List<MatOfPoint> matchedContours;
    private List<MatOfPoint> puntos;
    private MatOfPoint2f approxCurve;
    private Rect rect;

    private MatOfPoint2f contour2f;
    private MatOfPoint points;
    private ArrayList<Rect> rects;
    private Rect primero;
    private Rect segundo;
    private MatOfPoint puntosSegundo;
    private Point vectorPuntos[];

    private Rect rectangulo;
    private int contadorFPS = 0;
    private Mat mRgba;

    private double cantidadIngr;
    private double densidadIngr;
    private double alturaEnAgua;
    private double alturaEnAgua2;
    private double volumenSeekBar;

    private CameraBridgeViewBase mOpenCvCameraView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        flash = false;
        if (RecipienteSeleccionado.getInstance() == null){
            Toast.makeText(this,"Seleccioná un recipiente o agregalo", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, ConfiguracionActivity.class);
            startActivity(intent);
            finish();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marcar_nivel);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("Medir Ingrediente");
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);



        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.HelloOpenCvView);
        puntos = new ArrayList<MatOfPoint>();
        mOpenCvCameraView.setMaxFrameSize(640,480);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        jvc2 = (JavaCameraView2) findViewById(R.id.HelloOpenCvView);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progreso = progress;
                volumenSeekBar = Calculo.volumenEnAltura(RecipienteSeleccionado.getInstance(), progreso);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //Accion al tocar el boton mas recetas
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //finaliza la actividad de medir

            }
        });


        new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(fab))
                .setContentTitle("¿Cómo medir?")
                .setContentText("Apunta con tu cámara al recipiente que vas a usar para medir. Luego llena el recipiente con el ingrediente a medir, hasta la línea verde y presiona el botón")
                .singleShot(42)
                .build();



    }//Fin del oncreate

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {


        rgba = new Mat();
        editedFrame = new Mat();
        element = new Mat();
        contours = new ArrayList<>();
        matchedContours = new ArrayList<>();
        approxCurve = new MatOfPoint2f();
        rects = new ArrayList<>();

        rectangulo = new Rect(0,0,640,480);
        mRgba = new Mat();
        if (!flash) {
            flash = true;
            jvc2.setupCameraFlashLightOn();
        }


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            densidadIngr = extras.getDouble("DENSIDAD");
            cantidadIngr = extras.getDouble("CANT_INGR");

            alturaEnAgua = Calculo.porcentaje(Calculo.alturaEnRecipiente(RecipienteSeleccionado.getInstance(), Calculo.calcularCantidadIngregdiente(RecipienteSeleccionado.getInstance().getVolumen(), cantidadIngr, densidadIngr)), RecipienteSeleccionado.getInstance().getAltura());
            int cantidadRecipientesCompletos =  Calculo.cantidadRecipientesCompletos(cantidadIngr/densidadIngr,RecipienteSeleccionado.getInstance().getVolumen());
            if (cantidadRecipientesCompletos > 0){
                Toast.makeText(this, "Completar " + cantidadRecipientesCompletos + " recipientes y luego completar con el ingrediente hasta la lìnea verde", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(this, "Completar con el ingrediente hasta la lìnea verde",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onCameraViewStopped() {
        rgba.release();
        editedFrame.release();
        element.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        rgba = inputFrame.rgba();
        mRgba = rgba.submat(rectangulo);
        contours.clear();
        matchedContours.clear();
        rects.clear();
        puntos.clear();

        aplicarFiltros();

        Imgproc.findContours(editedFrame,contours, new Mat(),Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_TC89_KCOS);

        for (MatOfPoint contour : contours){
            if (Imgproc.contourArea(contour)>1000) //Descarto contornos chicos.
                matchedContours.add(contour);
        }

        if (matchedContours.size() != 1) { //Si hay menos dos de dos objetos, sigo capturando.
            return rgba;
        }

        for (int i = 0; i < matchedContours.size(); i++) {

            contour2f = new MatOfPoint2f(matchedContours.get(i).toArray());
            double approxDistance = Imgproc.arcLength(contour2f, true)*0.01;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
            points = new MatOfPoint(approxCurve.toArray());

            rect = Imgproc.boundingRect(points);
            rects.add(rect);
            puntos.add(points);

            Imgproc.rectangle(mRgba, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar( 0, 0, 255 ),0,20, 0);
        }

        if (rects.size() < 1) {
            return rgba; //Regresa la imagen, no hay un rectangulo.
        }

        marcaNivel(mRgba,rect, alturaEnAgua, new Scalar(0,255,0));

        if (seekBar.getProgress() != 0) {
            Imgproc.putText(rgba, String.format("Volumen[%.2f]", volumenSeekBar * densidadIngr), new Point(0,40), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);
            marcaNivel(mRgba, rect, progreso, new Scalar(0,0,255));
            System.out.println("Progreso es: "+progreso);
        }

        return rgba;
    }

    private Mat marcaNivel(Mat imagenOrigen, Rect contorno, double procentaje, Scalar color){

        double ancho = (double) (contorno.width);
        double alto = (contorno.height * (1.0 - (procentaje / 100)));

        Imgproc.line(imagenOrigen, new Point(contorno.x, contorno.y + alto),new Point(contorno.x + ancho, contorno.y + alto), color,5);

        return imagenOrigen;
    }

    private void aplicarFiltros(){
        Imgproc.cvtColor(mRgba, editedFrame,Imgproc.COLOR_RGBA2GRAY);
        Imgproc.GaussianBlur(editedFrame, editedFrame,new Size(15,15),0);
        Imgproc.Canny(editedFrame, editedFrame,500, 1500, 5, false);

        int dilation_size = 5;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*dilation_size + 1,2*dilation_size+1 ));
        Imgproc.dilate(editedFrame, editedFrame,element);

        int erotion_size = 5;
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*erotion_size+1,2*erotion_size+1));
        Imgproc.erode(editedFrame, editedFrame,element);

        editedFrame.copyTo(rgba.submat(rectangulo));
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();


    }
}


