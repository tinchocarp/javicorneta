package com.arsoft.cocinapp.modelo;


public class Recipiente {

    private int id;
    private double volumen;
    private String nombre;
    private String foto; //Esto va a tener una url a donde esta la foto del vaso.
    private double altura;
    private double ancho;
    private double radioChico;
    private double radioGrande;

    public Recipiente() {
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getRadioChico() {
        return radioChico;
    }

    public void setRadioChico(double radioChico) {
        this.radioChico = radioChico;
    }

    public double getRadioGrande() {
        return radioGrande;
    }

    public void setRadioGrande(double radioGrande) {
        this.radioGrande = radioGrande;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVolumen() {
        return volumen;
    }

    public void setVolumen(double volumen) {
        this.volumen = volumen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
