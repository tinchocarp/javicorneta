package com.arsoft.cocinapp.modelo;

public class RecipienteSeleccionado {
    private static Recipiente recipiente;

    private RecipienteSeleccionado() {
    }

    public static Recipiente getInstance() {
        return recipiente;
    }

    public static void setRecipiente(Recipiente recipiente){
        RecipienteSeleccionado.recipiente = recipiente;
    }
}
