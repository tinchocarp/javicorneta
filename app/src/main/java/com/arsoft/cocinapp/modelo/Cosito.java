package com.arsoft.cocinapp.modelo;

public class Cosito {
    private static Cosito ourInstance = new Cosito();

    private Double tamanio;

    private Cosito() {
        this.tamanio = 0.0;
    }

    public static Cosito getInstance() {
        return ourInstance;
    }

    public Double getTamanio() {
        return tamanio;
    }

    public void setTamanio(Double tamanio) {
        this.tamanio = tamanio;
    }
}
