package com.arsoft.cocinapp.modelo;

/**
 * Created by Porron on 6/8/16.
 */
public class IngredientePOJO {

    String nombre;
    Integer cantidad;
    String unidad;

    public IngredientePOJO(String nombre, Integer cantidad, String unidad) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.unidad = unidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IngredientePOJO that = (IngredientePOJO) o;

        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (cantidad != null ? !cantidad.equals(that.cantidad) : that.cantidad != null)
            return false;
        return unidad != null ? unidad.equals(that.unidad) : that.unidad == null;

    }

    @Override
    public int hashCode() {
        int result = nombre != null ? nombre.hashCode() : 0;
        result = 31 * result + (cantidad != null ? cantidad.hashCode() : 0);
        result = 31 * result + (unidad != null ? unidad.hashCode() : 0);
        return result;
    }
}
