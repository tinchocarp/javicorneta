package com.arsoft.cocinapp.modelo;


import java.io.Serializable;

public class Paso implements Serializable {

    private int id;
    private int idIngrediente;
    private String nombreIngrediente;
    private String unidad;
    private int medible;
    private int idReceta; // Con esto identifico a qué receta pertenecen los pasos
    private String descripcion;
    private int cantidadIngrediente;
    private String foto; //Va a tener una url a donde va a estar la foto.
    private String nombreOtroIngrediente;

    public Paso( String descripcion,String nombreIngrediente,int cantidadIngrediente,String unidad,int medible, String nombreOtroIngrediente) {

        this.descripcion = descripcion;
        this.cantidadIngrediente = cantidadIngrediente;
        this.nombreIngrediente = nombreIngrediente;
        this.unidad = unidad;
        this.medible = medible;
        this.nombreOtroIngrediente = nombreOtroIngrediente;
    }

    public Paso(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidadIngrediente() {
        return cantidadIngrediente;
    }

    public void setCantidadIngrediente(int cantidadIngrediente) {
        this.cantidadIngrediente = cantidadIngrediente;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombreIngrediente() {
        return nombreIngrediente;
    }

    public void setNombreIngrediente(String nombreIngrediente) {
        this.nombreIngrediente = nombreIngrediente;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public int getMedible() {
        return medible;
    }

    public void setMedible(int medible) {
        this.medible = medible;
    }

    public String getNombreOtroIngrediente() { return nombreOtroIngrediente;}

    public void setNombreOtroIngrediente(String nombreOtroIngrediente) { this.nombreOtroIngrediente = nombreOtroIngrediente; }
}
