package com.arsoft.cocinapp.modelo;


public class Receta {
    private int id;
    private String nombre;
    private String descripcion;
    private String fotoChica;
    private String fotoGrande;
    private String ingredientes;

    public Receta( String nombre, String descripcion, String fotoChica, String fotoGrande, String ingredientes) {

        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fotoChica = fotoChica;
        this.fotoGrande = fotoGrande;
        this.ingredientes = ingredientes;

    }
    public Receta() {
        this.fotoGrande = "na";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFotoChica() {
        return fotoChica;
    }

    public void setFotoChica(String fotoChica) {
        this.fotoChica = fotoChica;
    }

    public String getFotoGrande() {
        return fotoGrande;
    }

    public void setFotoGrande(String fotoGrande) {
        this.fotoGrande = fotoGrande;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }


}
