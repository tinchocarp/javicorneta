package com.arsoft.cocinapp.modelo;


public class Ingrediente {

    private int id;
    private String nombre;
    private String unidad;
    private float delta;


    public Ingrediente(int id, String nombre, String unidad, Float delta) {
        this.id=id;
        this.nombre = nombre;
        this.unidad = unidad;
        this.delta = delta;
    }
    public Ingrediente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public float getDelta() {
        return delta;
    }

    public void setDelta(float delta) {
        this.delta = delta;
    }

}
