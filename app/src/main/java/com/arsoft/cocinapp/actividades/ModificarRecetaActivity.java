package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.arsoft.cocinapp.modelo.Receta;
import com.arsoft.cocinapp.R;

import java.util.List;

import dbhelper.DatabaseHandler;
import dbhelper.ImageSaver;

public class ModificarRecetaActivity extends AppCompatActivity {

    public static final String EXTRA_POSITION = "position";
    public static String RECETA_ID = "RECETA_ID";
    DatabaseHandler db;
    List<Receta> recetas;
    Receta receta;
    ImageView recetaImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_receta);
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        //Declaro la base de datos
        db = new DatabaseHandler(this);
        //Traigo todas las recetas que hay cargadas en la bd.
        recetas = db.obtenerRecetas();


        //Se setea la información de la pantalla, se va a reemplazar por el contenido de las clases.
        final int postion = getIntent().getIntExtra(EXTRA_POSITION, 0);
        Resources resources = getResources();
        String[] places = resources.getStringArray(R.array.places);
        collapsingToolbar.setTitle(places[postion % places.length]);

        //obtiene de la BD la descripcion y lo pone en pantalla
        receta = recetas.get(postion);
        //setea titulo de la actividad
        collapsingToolbar.setTitle(receta.getNombre());

        final EditText descripcionReceta = (EditText) findViewById(R.id.desc_text);
        descripcionReceta.setText(receta.getDescripcion());

        final EditText ingredientesReceta = (EditText) findViewById(R.id.ingredientes_text);
        ingredientesReceta.setText(receta.getIngredientes());


        recetaImagen = (ImageView) findViewById(R.id.image);
        Bitmap bitmap;
        System.out.println("La fotoGrande es =("+receta.getFotoGrande().toLowerCase()+")");
        if(!receta.getFotoGrande().toLowerCase().equals("na")){
            //Si entro aca es porque la receta tiene una imagen cargada en la bd

            if(receta.getFotoGrande().toLowerCase().contains("@")){ //Si tiene @ es una imagen default
                String uri = receta.getFotoGrande();//Tiene que estar la imagen precargada en este formato: "@drawable/myresource";  // where myresource (without the extension) is the file

                int imageResource = getResources().getIdentifier(uri, null, getPackageName());

                Drawable res = getResources().getDrawable(imageResource);
                recetaImagen.setImageDrawable(res);

            }else{


                bitmap = new ImageSaver(getApplicationContext()).
                        setFileName(receta.getFotoGrande()).
                        setDirectoryName("images").
                        load();
                // Cargo la foto que acabo de sacar en el imageView
                recetaImagen.setImageBitmap(bitmap);
            }

        }

        FloatingActionButton fabConfirmar = (FloatingActionButton) findViewById(R.id.fabConfirmar);
        //Accion al tocar el boton mas recetas
        fabConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.modificarRecetaPrincipal(descripcionReceta,ingredientesReceta,postion);
                Intent intent = new Intent(v.getContext(),MainActivity.class);
                startActivity(intent);
                Toast.makeText(v.getContext(),"Los cambios se guardaron correctamente.", Toast.LENGTH_LONG).show();


            }
        });

        Button btnEditarPaso = (Button)findViewById(R.id.btnEditarPaso);
        btnEditarPaso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // aca va lo de iniciar receta
                Context context = v.getContext();
                final int visible = 1;
                Intent intent = new Intent(context, PasosActivity.class);
                intent.putExtra("KEY",receta.getId());
                intent.putExtra("KEYNombre",receta.getNombre());
                intent.putExtra("KEYEditar",visible);
                startActivity(intent);
            }
        });
    }
}
