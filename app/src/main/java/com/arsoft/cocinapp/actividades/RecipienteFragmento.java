package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arsoft.cocinapp.modelo.Recipiente;
import com.arsoft.cocinapp.modelo.RecipienteSeleccionado;
import com.bumptech.glide.Glide;
import com.arsoft.cocinapp.R;

import java.io.File;
import java.util.List;

import dbhelper.DatabaseHandler;

public class RecipienteFragmento extends Fragment {

    private static int[] androidColors;
    private static ContentAdapter adapter;
    private static RecyclerView recyclerView;


    @Override
    public void onResume(){
        super.onResume();
        //actualiza contenido
        adapter = new ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidColors = getResources().getIntArray(R.array.androidcolors);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);

        adapter = new ContentAdapter(recyclerView.getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        return recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView nombreRecipiente;
        private ImageButton fab;
        private ImageButton botonBorrar;


        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_recipiente_fragmento, parent, false));
            nombreRecipiente = (TextView) itemView.findViewById(R.id.card_text);
            picture = (ImageView) itemView.findViewById(R.id.card_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    DatabaseHandler db;
                    db = new DatabaseHandler(context);
                    RecipienteSeleccionado.setRecipiente(db.obtenerRecipientes().get(getAdapterPosition()));
                    Toast.makeText(context,"Seleccionó el recipiente ", Toast.LENGTH_LONG).show();

                    getActivity().finish();

                }
            });
            botonBorrar = (ImageButton) itemView.findViewById(R.id.boton_borrar);
            botonBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
            //Llamar al dialog
                    dialogoParaEliminar(v.getContext(),getAdapterPosition()).show();

                }
            }); //Fin de accion al buttonBorrar
        }

        public  AlertDialog dialogoParaEliminar(final Context context, final int posicion){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder
                    .setMessage("¿Desea eliminar el recipiente?")
                    .setPositiveButton("ELIMINAR",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Aca debo borrar el recipiente
                                    DatabaseHandler db;
                                    db = new DatabaseHandler(getContext());
                                    System.out.println("Se elimina el recipiente: "+db.obtenerRecipientes().get(getAdapterPosition()).getId());
                                    db.eliminarRecipiente(db.obtenerRecipientes().get(posicion).getId());

                                    //Actualizo la lista
                                    adapter = new ContentAdapter(recyclerView.getContext());
                                    recyclerView.setAdapter(adapter);

                                    dialog.cancel();
                                    Toast.makeText(context,"Se eliminó correctamente", Toast.LENGTH_LONG).show();
                                }
                            })
                    .setNegativeButton("CANCELAR",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

            return builder.create();
        }
    }

    /**
     * Adapter to display recycler view.
     */
    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {

        int LENGTH;
        private List<Recipiente> recipientes;
        private Context context;
        private String nombreRecipiente;
        private int imageResource;
        private Drawable res;

        public ContentAdapter(Context context) {

            DatabaseHandler db;

            db = new DatabaseHandler(context);
            recipientes = db.obtenerRecipientes();
            LENGTH = recipientes.size();

            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

//            nombreRecipiente = recipientes.get(position).getNombre().toLowerCase().replace(' ','_');
//            nombreRecipiente = Normalizer.normalize(nombreRecipiente, Normalizer.Form.NFD);
//            nombreRecipiente = nombreRecipiente.replaceAll("[^\\p{ASCII}]", "");
///*
            //imageResource = context.getResources().getIdentifier("@drawable/" + nombreRecipiente, null, context.getPackageName());
          //  File directory = context.getDir("images", Context.MODE_PRIVATE);
          //  Log.e("FotoGrende", recipientes.get(position).getFoto());
          //  Glide.with(context).load(new File(directory, recipientes.get(position).getFoto())).skipMemoryCache(true).into(holder.picture);

           // Glide.with(context).load(imageResource).into(holder.picture);
          //  holder.nombreRecipiente.setText( String.valueOf(recipientes.get(position).getVolumen()));
           /* if(!recipientes.isEmpty())
            if(!recipientes.get(position).getFoto().isEmpty()){
                //Si entro aca es porque la receta tiene una imagen cargada en la bd

                    File directory = context.getDir("images", Context.MODE_PRIVATE);
                    Log.e("FotoGrende", recipientes.get(position).getFoto());
                    Glide.with(context).load(new File(directory, recipientes.get(position).getFoto())).skipMemoryCache(true).into(holder.picture);

            }*/
            File directory = context.getDir("images", Context.MODE_PRIVATE);
           // Log.e("FotoGrende", recipientes.get(position).getFoto());
           Glide.with(context).load(new File(directory, recipientes.get(position).getFoto())).into(holder.picture);
            //Glide.with(context).load(recipientes.get(position).getFoto()).into(holder.picture);

//            holder.nombreRecipiente.setText(String.valueOf(recipientes.get(position).getVolumen()));
            holder.nombreRecipiente.setText(String.format("%.2f", recipientes.get(position).getVolumen()));
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
}
