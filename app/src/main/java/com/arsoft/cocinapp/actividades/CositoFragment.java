package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.modelo.Cosito;


public class CositoFragment extends Fragment {

    private Button btnMedir;
    private EditText medida;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_cosito, container, false);

        medida = (EditText) view.findViewById(R.id.medida_cosito);
        medida.setText(Cosito.getInstance().getTamanio().toString());
//        medida.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        btnMedir = (Button) view.findViewById(R.id.btnMedir);
        btnMedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!verificarCampos(view.getContext())){
                    return;
                }
                double tamanio = (Double.parseDouble(medida.getText().toString()));
                Cosito.getInstance().setTamanio(tamanio);
                Toast.makeText(view.getContext(),"Se guardó la medida", Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
    private boolean verificarCampos(Context context){

        if(medida.getText().toString().equalsIgnoreCase("")){
            Toast toast3 = Toast.makeText(context, "Falta ingresar la medida", Toast.LENGTH_SHORT);
            toast3.show();
            return false;
        }
        return true;

    }

/*

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ShowcaseView.Builder(getActivity())
                .withMaterialShowcase()
                //.setStyle(R.style.CustomShowcaseTheme2)
                .setTarget(new ViewTarget(button))
                .hideOnTouchOutside()
                .setContentTitle(R.string.showcase_fragment_title_2)
                .setContentText(R.string.showcase_fragment_message_2)
                .build();
    }

*/



}
