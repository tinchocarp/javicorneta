package com.arsoft.cocinapp.actividades;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arsoft.cocinapp.events.BotonEditarPasoHandler;
import com.arsoft.cocinapp.modelo.Ingrediente;
import com.arsoft.cocinapp.modelo.Paso;
import com.arsoft.cocinapp.opencv.MarcarNivelActivity;
import com.arsoft.cocinapp.R;

import java.util.ArrayList;
import java.util.List;

import dbhelper.DatabaseHandler;

public class PasosActivity extends AppCompatActivity {

    public static final String FIN_RECETA = "TERMINASTE LA RECETA";
    private static DatabaseHandler db;
    private static TextView textView;
    private List<Paso> listaPasos = new ArrayList<Paso>();
    private  int idReceta;
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        new AlertDialog.Builder(this)
                .setTitle("Cancelar Receta")
                .setMessage("¿Desea cancelar la receta?")
                .setNegativeButton("No", null)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create().show();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasos);

        //Declaro la base de datos
        db = new DatabaseHandler(this);

        //obtiene el id de la receta del activity anterior
        idReceta = getIntent().getExtras().getInt("KEY");
        String nombreR = getIntent().getExtras().getString("KEYNombre");

        System.out.println("idReceta tiene:" + idReceta);
        listaPasos = db.obtenerPasos(idReceta);

        /* Agregar paso FINAL*/
        Paso pasoFinal = new Paso();
        pasoFinal.setMedible(1);
        pasoFinal.setDescripcion(FIN_RECETA);
        listaPasos.add(pasoFinal);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(nombreR);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),listaPasos);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pasos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int medible;
        private List<Paso> listaPasos;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber,List<Paso> listaPasos) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            fragment.setListaPasos(listaPasos);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pasos, container, false);
            textView = (TextView) rootView.findViewById(R.id.section_label);

            //variable que me sirve para determinar cual de los dos activity lo llama
            final int esconderBotonModificar = getActivity().getIntent().getExtras().getInt("KEYEditar");
            System.out.println("Valor de esconderBotonModificar=" + esconderBotonModificar);

            //Setea el texto para cada paso

            textView.setText(listaPasos.get(getArguments().getInt(ARG_SECTION_NUMBER)-1).getDescripcion());

            Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/font.ttf");
            textView.setTypeface(face);

            Button btnCamara = (Button) rootView.findViewById(R.id.btnMedir);
            Paso pasoActual = listaPasos.get(getArguments().getInt(ARG_SECTION_NUMBER)-1);
            medible = pasoActual.getMedible();

            btnCamara.setVisibility(medible == 1 ? View.VISIBLE : View.INVISIBLE );
            if(!pasoActual.getDescripcion().equals(FIN_RECETA)){
                btnCamara.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<Ingrediente> ingredientes = db.obtenerIngredientes();
                        Ingrediente ingrediente =  ingredientes.get(listaPasos.get(getArguments().getInt(ARG_SECTION_NUMBER)-1).getIdIngrediente() - 1);

                        Intent intent = new Intent(getActivity().getApplicationContext(), MarcarNivelActivity.class);
                        intent.putExtra("DENSIDAD", Double.valueOf(ingrediente.getDelta()));
                        intent.putExtra("CANT_INGR", Double.valueOf(listaPasos.get(getArguments().getInt(ARG_SECTION_NUMBER)-1).getCantidadIngrediente()));
                        startActivity(intent);
                    }
                });
            }
            else{ //Aca hace el handler para volver a main
                btnCamara.setText("Finalizar");
                btnCamara.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().onBackPressed();
                    }
                });
            }


            FloatingActionButton btnEditar = (FloatingActionButton) rootView.findViewById(R.id.btnEditarPaso);
            btnEditar.setOnClickListener(new BotonEditarPasoHandler(listaPasos.get(getArguments().getInt(ARG_SECTION_NUMBER)-1)));
            btnEditar.setVisibility(esconderBotonModificar == 2 ? View.INVISIBLE : View.VISIBLE);

            return rootView;
        }

        public void setListaPasos(List<Paso> listaPasos) {
            this.listaPasos = listaPasos;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Paso> listaPasos;
        private long baseId = 0;

        public SectionsPagerAdapter(FragmentManager fm, List<Paso> listaPasos) {
            super(fm);
            this.listaPasos = listaPasos;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1,listaPasos);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return listaPasos.size();
        }

        @Override
        public int getItemPosition(Object object) {
            // refresh all fragments when data set changed
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public long getItemId(int position) {
            // give an ID different from position when position has been changed
            return baseId + position;
        }

        public void notifyChangeInPosition(int n) {
            // shift the ID returned by getItemId outside the range of all previous fragments
            baseId += getCount() + n;
        }

    }


}