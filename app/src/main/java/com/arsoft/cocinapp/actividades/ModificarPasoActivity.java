package com.arsoft.cocinapp.actividades;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.events.BotonConfirmModifHandler;
import com.arsoft.cocinapp.events.DataFragment;
import com.arsoft.cocinapp.events.SpinnerHandler;
import com.arsoft.cocinapp.modelo.Paso;

import dbhelper.DatabaseHandler;


public class ModificarPasoActivity extends AppCompatActivity {

    private  Paso paso;
    private  DatabaseHandler db;
    private ArrayAdapter<String> adaptador_ingredientes;
    private ArrayAdapter<String> adaptadormedidas;
    private TextInputLayout contenedor_nombre_ingrediente;
    private TextInputLayout contenedor_cantidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_paso);

        db = new DatabaseHandler(this);
        paso = (Paso) getIntent().getExtras().get("paso");
        AppCompatSpinner seleccion_ingrediente = (AppCompatSpinner)findViewById(R.id.spinner_ingrediente_edit);
        AppCompatSpinner seleccion_medida= (AppCompatSpinner)findViewById(R.id.spinner_medida_edit);
        EditText descripcion = (EditText)findViewById(R.id.campo_descripcion_edit);
        EditText nombreOtroIngrediente = (EditText)findViewById(R.id.campo_ingrediente_edit);
        EditText cantidad = (EditText)findViewById(R.id.campo_cantidad_edit);
        contenedor_cantidad = (TextInputLayout)findViewById(R.id.titulo_cantidad_edit);


        InicializarComponentes(seleccion_ingrediente,seleccion_medida,nombreOtroIngrediente);
        DataFragment dataFragment = new DataFragment(seleccion_ingrediente,seleccion_medida,descripcion,cantidad,nombreOtroIngrediente,adaptadormedidas,adaptador_ingredientes,contenedor_nombre_ingrediente,contenedor_cantidad);

        // Obtenego los datos de los spinner
        seleccion_ingrediente.setOnItemSelectedListener(new SpinnerHandler(dataFragment,false));

        seleccion_medida.setOnItemSelectedListener(new SpinnerHandler(dataFragment,true));

        FloatingActionButton btnConfirmar = (FloatingActionButton)findViewById(R.id.btnConfirmar);
        btnConfirmar.setOnClickListener(new BotonConfirmModifHandler(descripcion,paso,cantidad,seleccion_ingrediente,seleccion_medida, nombreOtroIngrediente));

    }


    private void InicializarComponentes(AppCompatSpinner seleccion_ingrediente,AppCompatSpinner seleccion_medida, EditText nombre) {
        String opciones_ingredientes[]={"Ingrediente","Agua","Harina","Leche","Café Molido","Coco Rallado","Cacao en Polvo","Maicena","Azúcar Negra","Crema","Azucar","Otro","Ninguno"};
        adaptador_ingredientes = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,opciones_ingredientes);
        seleccion_ingrediente.setAdapter(adaptador_ingredientes);

        String medidas[]={"Medida","g","ml","ninguna"};
        adaptadormedidas = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,medidas);
        seleccion_medida.setAdapter(adaptadormedidas);

        contenedor_nombre_ingrediente = (TextInputLayout)findViewById(R.id.titulo_ingrediente_text_edit);
        nombre.setVisibility(View.INVISIBLE);
        contenedor_nombre_ingrediente.setVisibility(View.INVISIBLE);

    }
}
