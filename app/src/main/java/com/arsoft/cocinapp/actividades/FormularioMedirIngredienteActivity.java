package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.modelo.Ingrediente;
import com.arsoft.cocinapp.opencv.MarcarNivelActivity;
import com.bumptech.glide.Glide;

import java.text.Normalizer;
import java.util.List;

import dbhelper.DatabaseHandler;

public class FormularioMedirIngredienteActivity extends AppCompatActivity {
    public static final String EXTRA_POSITION = "position";
    private DatabaseHandler db;
    private List<Ingrediente> ingredienteList;
    private Button btnMedir;
    private EditText cantidadIngrediente;
    private TextView txtUnidadMedida;
    private ImageView imagenIngrediente;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_ingrediente);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Set Collapsing Toolbar layout to the screen
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        int postion = getIntent().getIntExtra(EXTRA_POSITION, 0);
        Resources resources = getResources();
        String[] places = resources.getStringArray(R.array.places);
        collapsingToolbar.setTitle(places[postion % places.length]);


        //Declaro la base de datos
        db = new DatabaseHandler(this);
        //Traigo todas las recetas que hay cargadas en la bd.
        ingredienteList = db.obtenerIngredientes();


        //obtiene de la BD la descripcion y lo pone en pantalla
        final Ingrediente ingrediente = ingredienteList.get(postion);
        //setea titulo de la actividad
        collapsingToolbar.setTitle(ingrediente.getNombre());
        cantidadIngrediente = (EditText) findViewById(R.id.cantidadIngrediente);
//        cantidadIngrediente.setHint("Cantidad en " + ingrediente.getUnidad());

        String nombreIngrediente = ingrediente.getNombre().toLowerCase().replace(' ','_');
        nombreIngrediente = Normalizer.normalize(nombreIngrediente, Normalizer.Form.NFD);
        nombreIngrediente = nombreIngrediente.replaceAll("[^\\p{ASCII}]", "");
        int resID = getResources().getIdentifier(nombreIngrediente, "drawable",  getPackageName());

        imagenIngrediente = (ImageView) findViewById(R.id.image) ;

        int imageResource = getResources().getIdentifier("@drawable/" + nombreIngrediente, null, getPackageName());
        Glide.with(this).load(imageResource).into(imagenIngrediente);

        txtUnidadMedida = (TextView) findViewById(R.id.txtUnidadMedida);
        txtUnidadMedida.setText("Cantidad en " + ingrediente.getUnidad());


//        LLamada al Modulo de Realidad Auamentada
        btnMedir = (Button) findViewById(R.id.btnMedir);
        btnMedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!verificarCampos(getApplicationContext())){
                    return;
                }

                Intent intent = new Intent(getApplicationContext(), MarcarNivelActivity.class);
                intent.putExtra("DENSIDAD", Double.valueOf(ingrediente.getDelta()));
                intent.putExtra("CANT_INGR", Double.valueOf(cantidadIngrediente.getText().toString()));
                startActivity(intent);
            }
        });
    }


    private boolean verificarCampos(Context context){

        if(cantidadIngrediente.getText().toString().equalsIgnoreCase("")){
            Toast toast3 = Toast.makeText(context, "Falta ingresar la cantidad", Toast.LENGTH_SHORT);
            toast3.show();
            return false;

        }
        return true;

    }
}
