package com.arsoft.cocinapp.actividades;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.arsoft.cocinapp.events.BotonBorrarPasoHandler;
import com.arsoft.cocinapp.events.BotonSiguienteHandler;
import com.arsoft.cocinapp.events.ConfirmCrearRecetaHandler;
import com.arsoft.cocinapp.events.CrearRecetaHandler;
import com.arsoft.cocinapp.events.DataFragment;
import com.arsoft.cocinapp.events.SpinnerHandler;
import com.arsoft.cocinapp.modelo.Paso;
import com.arsoft.cocinapp.modelo.Receta;
import com.arsoft.cocinapp.R;

import java.util.ArrayList;
import java.util.List;

public class CrearRecetaPasosActivity extends AppCompatActivity {

    public static Button boton_finalizar;
    private  static TextView textView;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private static  SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private static ViewPager mViewPager;

    //agregado variables cris
    private List<Paso> listaPasos = new ArrayList<Paso>();
    private Receta receta;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        new AlertDialog.Builder(this)
                .setTitle("Cancelar Receta")
                .setMessage("¿Desea cancelar la receta?")
                .setNegativeButton("No", null)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create().show();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_receta);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        boton_finalizar = (Button)findViewById(R.id.btnFinalizarReceta);
        //obtenemos los datos del intent de la clase que nos llamo

        //obtiene el nombre de la receta del activity anterior
        String nombreReceta = getIntent().getExtras().getString("nombre");
        String descripcion = getIntent().getExtras().getString("descripcion");
        receta = new Receta();
        receta.setNombre(nombreReceta);
        receta.setDescripcion(descripcion);

        listaPasos.clear();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),listaPasos,receta);

        // Set up the ViewPager with the sections adapter.

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private List<Paso> listaPasos;

        private Receta receta;

        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, List<Paso> listaPasos,Receta receta) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            fragment.setListaPasos(listaPasos);
            fragment.setReceta(receta);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {

            AppCompatSpinner seleccion_ingrediente;
            AppCompatSpinner seleccion_medida;
            EditText  descripcion, cantidad, nombre_ingrediente;
            TextInputLayout contenedor_cantidad;

            final View rootView = inflater.inflate(R.layout.fragment_crear_receta, container, false);
            textView = (TextView) rootView.findViewById(R.id.section_label);

            contenedor_cantidad = (TextInputLayout)rootView.findViewById(R.id.titulo_cantidad);

            // spinners
            seleccion_ingrediente=(AppCompatSpinner)rootView.findViewById(R.id.spinner_ingrediente);
            seleccion_medida=(AppCompatSpinner)rootView.findViewById(R.id.spinner_medida);
            // campos de texto
            descripcion = (EditText)rootView.findViewById(R.id.campo_descripcion);
            nombre_ingrediente = (EditText)rootView.findViewById(R.id.campo_ingrediente);
            cantidad = (EditText)rootView.findViewById(R.id.campo_cantidad);

            // Escondo el campo para agregar un ingrediente a mano
            final TextInputLayout contenedor_nombre_ingrediente = (TextInputLayout)rootView.findViewById(R.id.titulo_ingrediente_text);
            nombre_ingrediente.setVisibility(View.INVISIBLE);
            contenedor_nombre_ingrediente.setVisibility(View.INVISIBLE);

            // Seteo los spinner con sus opciones
            final String opciones_ingredientes[]={"Ingrediente","Agua","Harina","Leche","Café Molido","Coco Rallado","Cacao en Polvo","Maicena","Azúcar Negra","Crema","Azucar","Otro","Ninguno"};
            final ArrayAdapter<String> adaptador_ingredientes = new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,opciones_ingredientes);
            seleccion_ingrediente.setAdapter(adaptador_ingredientes);

            String medidas[]={"Medida","g","ml","ninguna"};
            final ArrayAdapter<String> adaptadormedidas = new ArrayAdapter<String>(getContext()
                    ,R.layout.support_simple_spinner_dropdown_item,medidas);
            seleccion_medida.setAdapter(adaptadormedidas);

            //A partir de la informacion obtenida de los campos, creo este objeto para utilizarlo
            //en todos los eventos necesarios.
            DataFragment dataFragment = new DataFragment(seleccion_ingrediente,seleccion_medida,descripcion,cantidad,nombre_ingrediente,adaptadormedidas,adaptador_ingredientes,contenedor_nombre_ingrediente,contenedor_cantidad);

            // Obtenego los datos de los spinner
            seleccion_ingrediente.setOnItemSelectedListener(new SpinnerHandler(dataFragment,false));

            seleccion_medida.setOnItemSelectedListener(new SpinnerHandler(dataFragment,true));

            //Accion del boton siguiente paso
            Button boton_siguiente = (Button)rootView.findViewById(R.id.btnAgregarPaso);
            boton_siguiente.setOnClickListener(new BotonSiguienteHandler(listaPasos,dataFragment,mSectionsPagerAdapter,mViewPager));

            ImageButton boton_borrar = (ImageButton)rootView.findViewById(R.id.btnBorrar);
            boton_borrar.setOnClickListener(new BotonBorrarPasoHandler(listaPasos,mSectionsPagerAdapter,mViewPager,mViewPager.getCurrentItem(),getContext(),rootView));

            //acciones del boton finalizar
            boton_finalizar.setOnClickListener(new ConfirmCrearRecetaHandler(new CrearRecetaHandler(getContext(),receta,listaPasos)));


            return rootView;
        }

        public void setListaPasos(List<Paso> listaPasos) {
            this.listaPasos = listaPasos;
        }

        public void setReceta(Receta receta) {
            this.receta = receta;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private List<Paso> listaPasos;

        private Receta receta;

        private long baseId = 0;

        public SectionsPagerAdapter(FragmentManager fm, List<Paso> listaPasos,Receta receta) {
            super(fm);
            this.receta = receta;
            this.listaPasos = listaPasos;
        }

        @Override
        public Fragment getItem(int position) {
            // crea una instancia singleton de cada fragmento, para que no se pueda modificar
            // y le asigna un id para despues buscarlo y hacer los traspasos.
            return PlaceholderFragment.newInstance(position + 1,listaPasos,receta);
        }

        @Override
        public int getCount() {
            // muestra la cantidad de fragmentos activos.
            return listaPasos.size()+1;
        }

        @Override
        public int getItemPosition(Object object) {
            // refresh all fragments when data set changed
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public long getItemId(int position) {
            // give an ID different from position when position has been changed
            return baseId + position;
        }

        public void notifyChangeInPosition(int n) {
            // shift the ID returned by getItemId outside the range of all previous fragments
            baseId += getCount() + n;
        }

    }

}
