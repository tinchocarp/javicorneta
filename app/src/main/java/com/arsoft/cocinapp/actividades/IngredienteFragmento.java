/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.modelo.Ingrediente;
import com.bumptech.glide.Glide;

import java.text.Normalizer;
import java.util.List;

import dbhelper.DatabaseHandler;

/**
 * Provides UI for the view with Tile.
 */
public class IngredienteFragmento extends Fragment {

    private static int[] androidColors;
    private static ContentAdapter adapter;
    private static RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidColors = getResources().getIntArray(R.array.androidcolors);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);

        adapter = new ContentAdapter(recyclerView.getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        return recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView nombreIngrediente;
        private ImageButton fab;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_tile, parent, false));
            nombreIngrediente = (TextView) itemView.findViewById(R.id.card_text);
            picture = (ImageView) itemView.findViewById(R.id.card_image);

            Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/Notarized2.ttf");
            nombreIngrediente.setTypeface(face);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, FormularioMedirIngredienteActivity.class);
                    intent.putExtra(DescripcionRecetaActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }

    /**
     * Adapter to display recycler view.
     */
    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {

        int LENGTH;
        private List<Ingrediente> ingredientes;
        private Context context;
        private String nombreIngrediente;
        private int imageResource;
        private Drawable res;


        public ContentAdapter(Context context) {

            DatabaseHandler db;

            db = new DatabaseHandler(context);
            ingredientes = db.obtenerIngredientes();
            LENGTH = ingredientes.size();

            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            nombreIngrediente = ingredientes.get(position).getNombre().toLowerCase().replace(' ','_');
            nombreIngrediente = Normalizer.normalize(nombreIngrediente, Normalizer.Form.NFD);
            nombreIngrediente = nombreIngrediente.replaceAll("[^\\p{ASCII}]", "");

            imageResource = context.getResources().getIdentifier("@drawable/" + nombreIngrediente, null, context.getPackageName());
            Glide.with(context).load(imageResource).into(holder.picture);
            holder.nombreIngrediente.setText( ingredientes.get(position).getNombre());
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
}