package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arsoft.cocinapp.R;


public class CrearRecetaPrincipalActivity extends AppCompatActivity {

    private static EditText nombreReceta;
    private static EditText descripcion;
    private static Button boton;

    public static boolean verificarCampos(Context context){


        if(nombreReceta.getText().toString().equalsIgnoreCase("")){
            //Si entro aca es porque falta cargar descripcion
            Toast toast3 = Toast.makeText(context, "Falta ingresar el nombre", Toast.LENGTH_SHORT);
            toast3.show();
            return false;

        }
        if(descripcion.getText().toString().equalsIgnoreCase("")){
            //Si entro aca es porque falta cargar descripcion
            Toast toast3 = Toast.makeText(context, "Falta ingresar descripcion", Toast.LENGTH_SHORT);
            toast3.show();
            return false;
        }
        return true;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_receta1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        nombreReceta = (EditText) findViewById(R.id.campo_nombre);
        descripcion = (EditText) findViewById(R.id.campo_descripcion);



        boton = (Button) findViewById(R.id.button);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Accion boton comenzar a cargar pasos
                //Primero verifico
                if(!verificarCampos(getApplicationContext())){
                    return;
                }

                //Si llegamos aca es porque esta to do ok

                Intent intent = new Intent(getApplicationContext(), CrearRecetaPasosActivity.class);
                intent.putExtra("nombre", nombreReceta.getText().toString());
                intent.putExtra("descripcion",descripcion.getText().toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });


    }

}
