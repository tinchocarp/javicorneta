/*
 *Actividad para mostrar la descripcion de la receta
 * Uitiliza Collapsing Toolbar para realizar la interacción de la imagen
 * cuando se hace scroll en la pantalla
 */

package com.arsoft.cocinapp.actividades;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arsoft.cocinapp.modelo.Receta;
import com.arsoft.cocinapp.R;
import com.frosquivel.magicalcamera.MagicalCamera;

import java.util.List;

import dbhelper.DatabaseHandler;
import dbhelper.ImageSaver;


public class DescripcionRecetaActivity extends AppCompatActivity {

    public static final String EXTRA_POSITION = "position";
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    public static String RECETA_ID = "RECETA_ID";
    private static int RESIZE_PHOTO_PIXELS_PERCENTAGE = 3000;//Calidad de la foto a sacar, entre 50 y 4000
    private static MagicalCamera magicalCamera;
    List<Receta> recetas;
    DatabaseHandler db;
    Button botonTomarFoto;

    ImageView placePicture;
    Receta receta; //Esta variable va a tener la receta que cargamos en esta actividad

    Button btnIniciarReceta;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.descripcion_receta);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Set Collapsing Toolbar layout to the screen
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
         //Set title of Detail page
         //collapsingToolbar.setTitle(getString(R.string.item_title));



        //Esto es para la camara
        magicalCamera = new MagicalCamera(this,RESIZE_PHOTO_PIXELS_PERCENTAGE);
        //Declaro la base de datos
        db = new DatabaseHandler(this);
        //Traigo todas las recetas que hay cargadas en la bd.
        recetas = db.obtenerRecetas();


        //Se setea la información de la pantalla, se va a reemplazar por el contenido de las clases.
        int postion = getIntent().getIntExtra(EXTRA_POSITION, 0);
        Resources resources = getResources();
        String[] places = resources.getStringArray(R.array.places);
        collapsingToolbar.setTitle(places[postion % places.length]);

        Typeface face= Typeface.createFromAsset(getApplication().getAssets(), "fonts/Notarized2.ttf");
        //obtiene de la BD la descripcion y lo pone en pantalla
        receta = recetas.get(postion);
        //setea titulo de la actividad
        collapsingToolbar.setTitle(receta.getNombre());

        TextView descripcionTitulo = (TextView) findViewById(R.id.place_principal);
        descripcionTitulo.setTypeface(face);

        TextView ingredienteTitulo = (TextView) findViewById(R.id.ing_principal);
        ingredienteTitulo.setTypeface(face);

        TextView descripcionReceta = (TextView) findViewById(R.id.place_detail);
        descripcionReceta.setText(receta.getDescripcion());

        descripcionReceta.setTypeface(face);

        TextView ingredientesReceta = (TextView) findViewById(R.id.text_ingredientes);
        ingredientesReceta.setText(receta.getIngredientes());

        ingredientesReceta.setTypeface(face);


        placePicture = (ImageView) findViewById(R.id.image);


        btnIniciarReceta = (Button) findViewById(R.id.btnIniciarReceta);
        btnIniciarReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                final int visible = 2;
                Intent intent = new Intent(context, PasosActivity.class);
                intent.putExtra("KEY",receta.getId());
                intent.putExtra("KEYNombre",receta.getNombre());
                intent.putExtra("KEYEditar",visible);
                startActivity(intent);
            }
        });

        /*
        TypedArray placePictures = resources.obtainTypedArray(R.array.places_picture);

        placePicture.setImageDrawable(placePictures.getDrawable(postion % placePictures.length()));

        placePictures.recycle();
        */

        ///////// Hecho por gabi :

        //Me fijo si la receta tiene cargada alguna imagen en la bd.
        //Si tiene cargada una imagen, voy a cargarla de la memoria interna al imageView
        //Si no tiene, no cargo nada ya que se muestra una imagen default
        Bitmap bitmap;
        System.out.println("La fotoGrande es =("+receta.getFotoGrande().toLowerCase()+")");
        if(!receta.getFotoGrande().toLowerCase().equals("na")){
            //Si entro aca es porque la receta tiene una imagen cargada en la bd

            if(receta.getFotoGrande().toLowerCase().contains("@")){ //Si tiene @ es una imagen default
                String uri = receta.getFotoGrande();//Tiene que estar la imagen precargada en este formato: "@drawable/myresource";  // where myresource (without the extension) is the file

                int imageResource = getResources().getIdentifier(uri, null, getPackageName());

                Drawable res = getResources().getDrawable(imageResource);
                placePicture.setImageDrawable(res);

            }else{


            bitmap = new ImageSaver(getApplicationContext()).
            setFileName(receta.getFotoGrande()).
            setDirectoryName("images").
            load();
            // Cargo la foto que acabo de sacar en el imageView
            placePicture.setImageBitmap(bitmap);
            }

        }





        //Agrego accion al boton de la camara
        // Agrega el Floating Action Button a la vista principal
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //Accion al tocar el boton mas recetas
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

                        */
            //take photo
                //take photo
                magicalCamera.takePhoto();



            }
        });




    }



    //Activity result para obtener el resultado de la camara


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("El resultCode es="+resultCode); //Si es -1 se saco bien la foto. Si es 0, hubo error
        if(resultCode==0)
            return; //Hubo error o se presiono "atras"

        super.onActivityResult(requestCode, resultCode, data);

        //call this method ever
        magicalCamera.resultPhoto(requestCode, resultCode, data);

        Bitmap imagen = magicalCamera.getMyPhoto();
        //with this form you obtain the bitmap
      // Cargo la foto que acabo de sacar en el imageView
        placePicture.setImageBitmap(imagen);
        //Guardo la foto que acabo de sacar en la memoria interna

        new ImageSaver(getApplicationContext()).
                setFileName(receta.getId()+".png").
                setDirectoryName("images").
                save(imagen);


        System.out.println("Ya guarde la foto en la memoria interna");
        //Guardo la url de la foto en la bd
        db.agregarFotoGrandeReceta(receta.getId(),receta.getId()+".png");

        System.out.println("Ya agregue la foto en la db");

    }






    }








