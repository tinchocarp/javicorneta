package com.arsoft.cocinapp.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.opencv.ReconocimientoActivity;

import java.util.ArrayList;
import java.util.List;

public class ConfiguracionActivity extends AppCompatActivity {

    public static boolean fabVisible = true;
    ViewPager viewPager;
    private FloatingActionButton btnAgregarRecipiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        btnAgregarRecipiente = (FloatingActionButton) findViewById(R.id.fab);
        btnAgregarRecipiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReconocimientoActivity.class);
                startActivity(intent);
            }
        });

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() != 0){
                    fabVisible = false;
                    btnAgregarRecipiente.hide();
                }
                else {
                    fabVisible = true;
                    btnAgregarRecipiente.show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


/*
            //Comentamos lo siguiente porque no anda bien, siempre aparece el boton OK
            new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ViewTarget(btnAgregarRecipiente))
                    //.setStyle(R.style.CustomShowcaseTheme2)
                    .setContentText("\n\n\n" +
                            "Para agregar un nuevo recipiente: Ir a la sección Referencia e indicar el ancho (en centímetros) del objeto de referencia.")
                    .singleShot(43)
                    .build()
                    .show();

*/




    }//Llave fin del oncreate

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new RecipienteFragmento(), "Recipientes");
        adapter.addFragment(new CositoFragment(), "Referencia");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
