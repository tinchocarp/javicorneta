package com.arsoft.cocinapp.actividades;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.modelo.Ingrediente;
import com.arsoft.cocinapp.modelo.Receta;

import java.util.ArrayList;
import java.util.List;

import dbhelper.DatabaseHandler;


/**
 * Provee UI a interfaz principal
 */
public class MainActivity extends AppCompatActivity {

    private static final int SOLICITUD_PERMISO_CAMERA = 0;
    private static final int SOLICITUD_PERMISO_WRITE_EXTERNAL_STORAGE = 1;
    public static boolean fabVisible = true;
    List<Receta> recetas = new ArrayList<>();
    List<Ingrediente> ingredientes = new ArrayList<>();
    DatabaseHandler db;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Cerrar CocinApp")
                .setMessage("¿Está seguro que desea salir?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        System.exit(0);
                    }
                }).create().show();
    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        return false;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Agregar Toolbar a la pantalla principal
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        SOLICITUD_PERMISO_WRITE_EXTERNAL_STORAGE);

            }
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        SOLICITUD_PERMISO_CAMERA);

            }
        }

        // Agrega el Floating Action Button a la vista principal

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //Accion al tocar el boton mas recetas
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, CrearRecetaPrincipalActivity.class);
                context.startActivity(intent);
            }
        });

        // Setea ViewPager para cada Tabs
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);// inicializa todos los fragmentos con el Adapter(configuracionVistaPagina)
        // Setea Tabs adentro de Toolbar
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager); //a cada tab le setea el contenido de viewPager


        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() != 0){
                    fabVisible = false;
                    fab.hide();
                }
                else {
                    fabVisible = true;
                    fab.show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //Declaro la base de datos
        db = new DatabaseHandler(this).inicializar();

        //Traigo todas las recetas que hay cargadas en la bd.
         recetas = db.obtenerRecetas();
         ingredientes = db.obtenerIngredientes();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SOLICITUD_PERMISO_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            } case SOLICITUD_PERMISO_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }

    // Agrega los fragmentos a las tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new RecetaFragmento(), "Recetas");
        adapter.addFragment(new IngredienteFragmento(), "Ingredientes");
        viewPager.setAdapter(adapter); //setea el contenido de la Pagina con todos los fragmentos
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.acerca_de:
                Toast.makeText(this,"CocinApp.2016.Argentina.Todos los derechos reservados.", Toast.LENGTH_LONG).show();
                return true;
            case R.id.configuracion:
                Intent intent = new Intent(this, ConfiguracionActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.ayuda:
                Toast.makeText(this,"Voy a Ayuda", Toast.LENGTH_LONG).show();
                return true;
            case R.id.salir:
                new AlertDialog.Builder(this)
                        .setTitle("Cerrar CocinApp")
                        .setMessage("¿Está seguro que desea salir?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                finish();
                                System.exit(0);
                            }
                        }).create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }



    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
