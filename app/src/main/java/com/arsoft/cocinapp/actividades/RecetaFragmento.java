package com.arsoft.cocinapp.actividades;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arsoft.cocinapp.R;
import com.arsoft.cocinapp.modelo.Receta;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dbhelper.DatabaseHandler;

/**
 * Provee la UI de la vista con Cards.
 * El recycler view es un contenedor que se usa en los fragmentos para poder mostrar las informaciones
 * de distintas maneras segun el estilo que se use el cual es especificado en el XML, en este caso para las recetas
 * usamos el diseño cardview que presenta la información en forma de tarjetas.
 *
 */
public class RecetaFragmento extends Fragment {

    private static List<Receta> recetas;
    private static ContentAdapter adapter;
    private static RecyclerView recyclerView;
    private String descripcionReceta;

    public AlertDialog dialogoParaEliminar(final Context context, final int posicion){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder
                .setMessage("¿Desea eliminar la receta "+ recetas.get(posicion).getNombre() + "?")
                .setPositiveButton("ELIMINAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Receta recetaAeliminar = recetas.get(posicion);
                                recetas.remove(recetaAeliminar);
                                DatabaseHandler db = new DatabaseHandler(context);
                                db.eliminarReceta(recetaAeliminar.getId());
                                db.eliminarPasosReceta(recetaAeliminar.getId());
                                //actualiza contenido
                                adapter = new ContentAdapter(recyclerView.getContext());
                                recyclerView.setAdapter(adapter);

                                //Elimino la receta de la bd

                                dialog.cancel();
                                Toast.makeText(context,"Se eliminó correctamente", Toast.LENGTH_LONG).show();
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);


        adapter = new ContentAdapter(recyclerView.getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return recyclerView;
    }

    @Override
    public void onResume(){
        super.onResume();

        //actualiza contenido
        adapter = new ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        public TextView nombreReceta;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_card, parent, false));
            picture = (ImageView) itemView.findViewById(R.id.card_image);
            nombreReceta = (TextView) itemView.findViewById(R.id.card_text);


            Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "fonts/Notarized2.ttf");
            nombreReceta.setTypeface(face);
            //Abre DescripcionRecetaActivity
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Context context = v.getContext();
                    Intent intent = new Intent(context, DescripcionRecetaActivity.class);
                    intent.putExtra(DescripcionRecetaActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);


                }
            });

            // botones de la tarjeta, no se van a usar

            final ImageButton favoriteImageButton =
                    (ImageButton) itemView.findViewById(R.id.favorite_button);
            favoriteImageButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {

                    Context context = v.getContext();

                    dialogoParaEliminar(context,getAdapterPosition()).show();
                }
            });

            ImageButton shareImageButton = (ImageButton) itemView.findViewById(R.id.share_button);
            shareImageButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {

                    Context context = v.getContext();

                    picture.setDrawingCacheEnabled(true);
                    picture.buildDrawingCache();
                    Bitmap bitmap = picture.getDrawingCache();

                    try {
                        File cachePath = new File(context.getCacheDir(), "images");
                        cachePath.mkdirs();
                        FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File imagePath = new File(context.getCacheDir(), "images");
                    File newFile = new File(imagePath, "image.png");
                    Uri contentUri = FileProvider.getUriForFile(context, "com.arsoft.cocinapp", newFile);

                    if (contentUri != null) {

                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                        shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
                        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hacelo con CocinApp");

                        context.startActivity(Intent.createChooser(shareIntent, "Selecciona una aplicacion para compartir"));

                        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("Descipción", recetas.get(getAdapterPosition()).getDescripcion() + " Hacelo con CocinApp!");
                        clipboard.setPrimaryClip(clip);

                    }

                }
            });

            ImageButton editImageButton = (ImageButton) itemView.findViewById(R.id.edit_button);
            editImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context,ModificarRecetaActivity.class);
                    intent.putExtra(DescripcionRecetaActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);

                }
            });
        }
    }

    /**
     * Adapter para mostrar el recycler view.
     */
    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        private final Drawable[] mPlacePictures;
        private final DatabaseHandler db;
        private final List<Drawable> listaDinamicaImagenes = new ArrayList<>();
        // Setea una cantidad de cards a mostrar, no se va a utilizar
        int LENGTH;
        private Context context;

        //obtener las imagenes de de la BD
        public ContentAdapter(Context context) {
            //mepa que cualquiera
            System.out.println("Se ejecuto el metodo contentadapter (???)");

            //se obtiene la primera vez las recetas de la BD, despues se maneja con la lista.
            db = new DatabaseHandler(context);
            recetas = db.obtenerRecetas();

            LENGTH = recetas.size();

            /////este codigo no sirve, lo dejo para que por ahora muestre imagenes en las tarjetas
            Resources resources = context.getResources();
            TypedArray a = resources.obtainTypedArray(R.array.places_picture);
            mPlacePictures = new Drawable[a.length()];

            //cargar en listaDinamicaImagenes la imagen de la receta que va a mostrarse en el card
            for (int i = 0; i < LENGTH; i++) {
                mPlacePictures[i] = a.getDrawable(i);
                listaDinamicaImagenes.add(a.getDrawable(i));
            }

            this.context = context;

            a.recycle();
            /////fin de codigo que no sirve
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        //Metodo que setea los valores de cada card
        //Asocia las informaciones a cada variable XML de las cards
        public void onBindViewHolder(ViewHolder holder, int position) {
            Receta receta = recetas.get(position);

            if(!receta.getFotoGrande().toLowerCase().equals("na")){
                //Si entro aca es porque la receta tiene una imagen cargada en la bd
                if(receta.getFotoGrande().toLowerCase().contains("@")){ //Si tiene @ es una imagen default
                    String uri = receta.getFotoGrande();//Tiene que estar la imagen precargada en este formato: "@drawable/myresource";  // where myresource (without the extension) is the file
                    int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
                    Glide.with(context).load(imageResource).into(holder.picture);
                }else {
                    File directory = context.getDir("images", Context.MODE_PRIVATE);
                    Log.e("FotoGrende", receta.getFotoGrande());
                    Glide.with(context).load(new File(directory, receta.getFotoGrande())).skipMemoryCache(true).into(holder.picture);
                }
            }else {
                //Si entro aca es porque tengo que cargar la imagen default
                String uri = "@drawable/androidcooking";  // where myresource (without the extension) is the file
                int imageResource = holder.picture.getContext().getResources().getIdentifier(uri, null, holder.picture.getContext().getPackageName());
                Glide.with(context).load(imageResource).into(holder.picture);
            }
            holder.nombreReceta.setText(receta.getNombre());
//            descripcionReceta = receta.getDescripcion();
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
}
