package com.arsoft.cocinapp.events;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Porron on 24/9/16.
 */
public class SpinnerHandler implements AdapterView.OnItemSelectedListener{

    private boolean isMedida;

    private DataFragment dataFragment;

    public SpinnerHandler(DataFragment dataFragment, boolean isMedida){
        this.dataFragment = dataFragment;
        this.isMedida = isMedida;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(isMedida){
            dataFragment.setOpcion_medida(dataFragment.getAdaptadormedidas().getItem(position));
        }else {
            dataFragment.setOpcion_ingrediente(dataFragment.getAdaptador_ingredientes().getItem(position));
            if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("ninguno")) {
                dataFragment.getNombre_ingrediente().setVisibility(View.INVISIBLE);
                dataFragment.getContenedor_nombre_ingrediente().setVisibility(View.INVISIBLE);
                dataFragment.getContenedor_cantidad().setVisibility(View.INVISIBLE);
                dataFragment.getCantidad().setVisibility(View.INVISIBLE);
                dataFragment.getSeleccion_medida().setVisibility(View.INVISIBLE);

            } else {
                if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("otro")) {
                    dataFragment.getNombre_ingrediente().setVisibility(View.VISIBLE);
                    dataFragment.getContenedor_nombre_ingrediente().setVisibility(View.VISIBLE);
                } else {
                    dataFragment.getNombre_ingrediente().setVisibility(View.INVISIBLE);
                    dataFragment.getContenedor_nombre_ingrediente().setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
