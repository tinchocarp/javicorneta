package com.arsoft.cocinapp.events;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.arsoft.cocinapp.modelo.Recipiente;
import com.arsoft.cocinapp.utilitarios.Calculo;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.LinkedList;
import java.util.List;

import dbhelper.DatabaseHandler;
import dbhelper.ImageSaver;

/**
 * Created by PC on 29/9/2016.
 */
public class GuardarRecipienteHandler implements DialogInterface.OnClickListener {

    private Context context;
    private LinkedList<Recipiente> recipientes;
    private Mat rgba;
    private int idUltimoRecipiente=0;
    private List<Recipiente> listaRecipienteID;

    public GuardarRecipienteHandler(Context context, LinkedList<Recipiente> recipientes, Mat rgba){
        this.context = context;
        this.recipientes = recipientes;
        this.rgba=rgba;

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        DatabaseHandler db = new DatabaseHandler(context);

        db.agregarRecipiente(obtenerRecipientePromedio());
        dialog.cancel();
        Imgproc.cvtColor(rgba,rgba , Imgproc.COLOR_GRAY2RGBA,4); // add this

        ((Activity)context).finish();
    }



    private void guardarfoto() {
        Bitmap imagen = Bitmap.createBitmap(rgba.width(),rgba.height(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(rgba,imagen);
        new ImageSaver(context).
                setFileName("Recipiente"+idUltimoRecipiente+".png").
                setDirectoryName("images").
                save(imagen);

    }

    public AlertDialog dialogoParaGuardarRecipiente(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder
                .setMessage("¿Desea guardar el recipiente ?")
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DatabaseHandler db = new DatabaseHandler(context);
                                db.agregarRecipiente(obtenerRecipientePromedio());
                                listaRecipienteID = db.obtenerRecipientes();
                                idUltimoRecipiente=listaRecipienteID.get(listaRecipienteID.size()-1).getId();
                                db.modificarFotoRecipiente("Recipiente"+idUltimoRecipiente+".png",idUltimoRecipiente);
                                guardarfoto();

                                dialog.cancel();
                                Toast.makeText(context,"Se guardó correctamente el recipiente.", Toast.LENGTH_LONG).show();
                                Activity act = (Activity)context;
                                act.finish();
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        return builder.create();
    }

    private Recipiente obtenerRecipientePromedio(){
        Recipiente recipienteAcumulado = new Recipiente();
        for (Recipiente recipiente : recipientes){
            recipienteAcumulado.setAltura(recipienteAcumulado.getAltura() + recipiente.getAltura());
            recipienteAcumulado.setAncho(recipienteAcumulado.getAncho() + recipiente.getAncho());
            recipienteAcumulado.setRadioChico(recipienteAcumulado.getRadioChico() + recipiente.getRadioChico());
            recipienteAcumulado.setRadioGrande(recipienteAcumulado.getRadioGrande() + recipiente.getRadioGrande());
            recipienteAcumulado.setVolumen(recipienteAcumulado.getVolumen() + recipiente.getVolumen());
        }

        recipienteAcumulado.setAltura(recipienteAcumulado.getAltura() / recipientes.size());
        recipienteAcumulado.setAncho(recipienteAcumulado.getAncho() / recipientes.size());
        recipienteAcumulado.setRadioChico(recipienteAcumulado.getRadioChico() / recipientes.size());
        recipienteAcumulado.setRadioGrande(recipienteAcumulado.getRadioGrande() / recipientes.size());
        recipienteAcumulado.setVolumen(Calculo.obtenerMedidas(recipienteAcumulado.getAltura(),recipienteAcumulado.getRadioChico(),recipienteAcumulado.getRadioGrande()));

        return recipienteAcumulado;
    }
}
