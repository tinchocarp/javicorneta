package com.arsoft.cocinapp.events;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.arsoft.cocinapp.actividades.CrearRecetaPasosActivity;
import com.arsoft.cocinapp.modelo.Paso;
import com.arsoft.cocinapp.R;

import java.util.List;

/**
 * Created by Porron on 20/10/16.
 */
public class BotonBorrarPasoHandler implements View.OnClickListener {

    private final View rootView;
    private Context context;
    private int currentItem;
    private List<Paso> listaPasos;
    private CrearRecetaPasosActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    public BotonBorrarPasoHandler(List<Paso> listaPasos, CrearRecetaPasosActivity.SectionsPagerAdapter mSectionsPagerAdapter, ViewPager mViewPager, int currentItem, Context context, View rootView){
        this.context = context;
        this.currentItem = currentItem;
        this.listaPasos = listaPasos;
        this.mViewPager = mViewPager;
        this.mSectionsPagerAdapter = mSectionsPagerAdapter;
        this.rootView = rootView;
    }

    @Override
    public void onClick(View v) {
        if( currentItem == listaPasos.size() ){
            Toast.makeText(context,"El formulario no puede ser borrado",Toast.LENGTH_SHORT).show();
        }else{
            listaPasos.remove(currentItem);
            AppCompatSpinner seleccion_ingrediente = (AppCompatSpinner) rootView.findViewById(R.id.spinner_ingrediente);
            AppCompatSpinner seleccion_medida = (AppCompatSpinner) rootView.findViewById(R.id.spinner_medida);
            // campos de texto
            EditText descripcion = (EditText) rootView.findViewById(R.id.campo_descripcion);
            EditText nombre_ingrediente = (EditText) rootView.findViewById(R.id.campo_ingrediente);
            EditText cantidad = (EditText) rootView.findViewById(R.id.campo_cantidad);
            seleccion_ingrediente.setSelection(0);
            seleccion_medida.setSelection(0);
            descripcion.setText("");
            nombre_ingrediente.setText("");
            cantidad.setText("");
            mSectionsPagerAdapter.notifyDataSetChanged();
            mViewPager.setCurrentItem(listaPasos.size());
        }
    }
}
