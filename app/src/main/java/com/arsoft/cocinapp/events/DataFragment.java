package com.arsoft.cocinapp.events;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ArrayAdapter;
import android.widget.EditText;

/**
 * Created by Porron on 24/9/16.
 */
public class DataFragment {

    private final ArrayAdapter<String> adaptadormedidas;
    private final ArrayAdapter<String> adaptador_ingredientes;
    private AppCompatSpinner seleccion_ingrediente;
    private AppCompatSpinner seleccion_medida;
    private String opcion_ingrediente;
    private String opcion_medida;
    private EditText descripcion;
    private EditText cantidad;
    private EditText nombre_ingrediente;
    private TextInputLayout contenedor_nombre_ingrediente;
    private TextInputLayout contenedor_cantidad;

    public DataFragment(AppCompatSpinner seleccion_ingrediente, AppCompatSpinner seleccion_medida, EditText descripcion, EditText cantidad, EditText nombre_ingrediente, ArrayAdapter<String> adaptadormedidas, ArrayAdapter<String> adaptador_ingredientes,TextInputLayout contenedor_nombre_ingrediente, TextInputLayout contenedor_cantidad) {
        this.seleccion_ingrediente = seleccion_ingrediente;
        this.seleccion_medida = seleccion_medida;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.nombre_ingrediente = nombre_ingrediente;
        this.adaptadormedidas = adaptadormedidas;
        this.adaptador_ingredientes = adaptador_ingredientes;
        this.contenedor_nombre_ingrediente = contenedor_nombre_ingrediente;
        this.contenedor_cantidad = contenedor_cantidad;
    }

    public AppCompatSpinner getSeleccion_ingrediente() {
        return seleccion_ingrediente;
    }

    public void setSeleccion_ingrediente(AppCompatSpinner seleccion_ingrediente) {
        this.seleccion_ingrediente = seleccion_ingrediente;
    }

    public AppCompatSpinner getSeleccion_medida() {
        return seleccion_medida;
    }

    public void setSeleccion_medida(AppCompatSpinner seleccion_medida) {
        this.seleccion_medida = seleccion_medida;
    }

    public String getOpcion_ingrediente() {
        return opcion_ingrediente;
    }

    public void setOpcion_ingrediente(String opcion_ingrediente) {
        this.opcion_ingrediente = opcion_ingrediente;
    }

    public String getOpcion_medida() {
        return opcion_medida;
    }

    public void setOpcion_medida(String opcion_medida) {
        this.opcion_medida = opcion_medida;
    }

    public EditText getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(EditText descripcion) {
        this.descripcion = descripcion;
    }

    public EditText getCantidad() {
        return cantidad;
    }

    public void setCantidad(EditText cantidad) {
        this.cantidad = cantidad;
    }

    public EditText getNombre_ingrediente() {
        return nombre_ingrediente;
    }

    public void setNombre_ingrediente(EditText nombre_ingrediente) {
        this.nombre_ingrediente = nombre_ingrediente;
    }

    public ArrayAdapter<String> getAdaptadormedidas() {
        return adaptadormedidas;
    }

    public ArrayAdapter<String> getAdaptador_ingredientes() {
        return adaptador_ingredientes;
    }

    public TextInputLayout getContenedor_nombre_ingrediente() {
        return contenedor_nombre_ingrediente;
    }

    public void setContenedor_nombre_ingrediente(TextInputLayout contenedor_nombre_ingrediente) {
        this.contenedor_nombre_ingrediente = contenedor_nombre_ingrediente;
    }

    public TextInputLayout getContenedor_cantidad(){
        return contenedor_cantidad;
    }

    public void setContenedor_cantidad(TextInputLayout contenedor_cantidad){
        this.contenedor_cantidad = contenedor_cantidad;
    }
}
