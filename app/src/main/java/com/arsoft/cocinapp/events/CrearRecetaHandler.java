package com.arsoft.cocinapp.events;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.arsoft.cocinapp.actividades.MainActivity;
import com.arsoft.cocinapp.modelo.Paso;
import com.arsoft.cocinapp.modelo.Receta;
import com.arsoft.cocinapp.utilitarios.CalculoIngredientes;

import java.util.ArrayList;
import java.util.List;

import dbhelper.DatabaseHandler;


/**
 * Created by Porron on 16/9/16.
 * Esta clase se encarga de crear el dialogo y guardar en la base la receta creada.
 */
public class CrearRecetaHandler implements DialogInterface.OnClickListener {

    private Context context;

    private Receta receta;

    private List<Paso> listaPasos;

    public CrearRecetaHandler(Context context, Receta receta, List<Paso> listaPasos){
        this.context = context;
        this.receta = receta;
        this.listaPasos = listaPasos;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        // Guardará los datos del paso actual y confirmará todos los otros datos guardados parcialmente
        DatabaseHandler db = new DatabaseHandler(context);
        ArrayList<Paso> listaFinal = new ArrayList<Paso>();

        //Le cargo el id de la receta a los pasos
        int idReceta = db.agregarReceta(receta);
        for (int i = 0; i < listaPasos.size(); i++) {
            Paso o =  listaPasos.get(i);
            o.setIdReceta(idReceta);
            listaFinal.add(o);
        }

        db.agregarPasos(listaFinal);

        dialog.cancel();
        Toast.makeText(context,"Se creó correctamente la receta.", Toast.LENGTH_LONG).show();
        listaPasos.clear();
        Intent intent = new Intent(context,MainActivity.class);
        context.startActivity(intent);

    }

    public AlertDialog dialogoParaFinalizarReceta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder
                .setMessage("¿Desea finalizar la receta ?")
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Guardará los datos del paso actual y confirmará todos los otros datos guardados parcialmente
                                DatabaseHandler db = new DatabaseHandler(context);
                                ArrayList<Paso> listaFinal = new ArrayList<Paso>();

                                //Le cargo el id de la receta a los pasos
                                int idReceta = db.agregarReceta(receta);
                                for (int i = 0; i < listaPasos.size(); i++) {
                                    Paso o =  listaPasos.get(i);
                                    o.setIdReceta(idReceta);
                                    listaFinal.add(o);
                                }
                                db.agregarPasos(listaFinal);

                                String descripcion = String.format("%s \n %s",receta.getDescripcion(),CalculoIngredientes.crearListaIngredientes(listaFinal));
                                db.modificarDescripcionReceta(descripcion,idReceta);

                                dialog.cancel();
                                Toast.makeText(context,"Se creó correctamente la receta.", Toast.LENGTH_LONG).show();
                                listaPasos.clear();
                                Intent intent = new Intent(context,MainActivity.class);
                                context.startActivity(intent);
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        return builder.create();
    }
}
