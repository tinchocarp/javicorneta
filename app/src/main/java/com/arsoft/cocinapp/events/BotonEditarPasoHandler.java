package com.arsoft.cocinapp.events;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.arsoft.cocinapp.actividades.ModificarPasoActivity;
import com.arsoft.cocinapp.modelo.Paso;

/**
 * Created by Cristian on 08/10/2016.
 */

public class BotonEditarPasoHandler implements View.OnClickListener {
    private Paso paso;

    public BotonEditarPasoHandler(Paso paso) {
        this.paso = paso;
    }

    @Override
    public void onClick(View v) {
        Context context = v.getContext();
        Intent intent2 = new Intent(context,ModificarPasoActivity.class);
        intent2.putExtra("paso",paso);
        context.startActivity(intent2);
    }

}
