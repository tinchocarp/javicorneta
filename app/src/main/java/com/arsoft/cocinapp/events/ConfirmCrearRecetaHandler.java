package com.arsoft.cocinapp.events;

import android.view.View;

/**
 * Created by Porron on 23/9/16.
 * Esta clase se utiliza para mostrar el dialogo de finalizar receta. Era necesario crearlo asi
 * porque son distintas interfaces (Entre confirm y crear)
 */
public class ConfirmCrearRecetaHandler implements View.OnClickListener {

    private CrearRecetaHandler crh;

    public ConfirmCrearRecetaHandler(CrearRecetaHandler crh){
        this.crh = crh;
    }

    @Override
    public void onClick(View v) {
        crh.dialogoParaFinalizarReceta().show();
    }
}
