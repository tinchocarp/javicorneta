package com.arsoft.cocinapp.events;

import android.view.View;

/**
 * Created by PC on 29/9/2016.
 */
public class ConfirmGuardarRecipienteHandler implements View.OnClickListener  {
    private GuardarRecipienteHandler guardarRecipienteHandler;

    public ConfirmGuardarRecipienteHandler(GuardarRecipienteHandler guardarRecipienteHandler){
        this.guardarRecipienteHandler = guardarRecipienteHandler;
    }

    @Override
    public void onClick(View v) {
        guardarRecipienteHandler.dialogoParaGuardarRecipiente().show();
    }
}
