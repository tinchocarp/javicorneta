package com.arsoft.cocinapp.events;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.arsoft.cocinapp.actividades.CrearRecetaPasosActivity;
import com.arsoft.cocinapp.modelo.Paso;

import java.util.List;

/**
 * Created by Porron on 23/9/16.
 * Esta clase es el handler para cuando se toca siguiente, valida el formulario, luego guarda la
 * lista en memoria (java pasa objetos por referencia por lo tanto la lista siempre es la misma porque
 * no se les hace new en ninguna instancia salvo en la activity de crear receta).
 * Despues notifica al fragment manager del cambio asi genera uno nuevo
 * y le pasa la informacion
 */
public class BotonSiguienteHandler implements View.OnClickListener {

    private List<Paso> listaPasos;

    private DataFragment dataFragment;

    private boolean camposIncompletos; //los boolean por defecto son Falsos.

    private CrearRecetaPasosActivity.SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    public BotonSiguienteHandler(List<Paso> listaPasos, DataFragment dataFragment, CrearRecetaPasosActivity.SectionsPagerAdapter mSectionsPagerAdapter, ViewPager mViewPager){
        this.listaPasos = listaPasos;
        this.dataFragment = dataFragment;
        this.mViewPager = mViewPager;
        this.mSectionsPagerAdapter = mSectionsPagerAdapter;
    }

    @Override
    public void onClick(View v) {
        // flag para determinar si puedo continuar con el siguiente paso o hay algun campo incompleto

        String descripcionPaso;
        String nombreIngredientePaso = "";
        String nombreOtroIngredientePaso = "";
        int esMedible = 0; // no es medible
        int cantidadIngredientePaso;
        String unidadPaso = "";
        verificarCampos(v.getContext());

        if(!camposIncompletos) {
            descripcionPaso = dataFragment.getDescripcion().getText().toString();


            if(dataFragment.getOpcion_ingrediente().equalsIgnoreCase("ninguno")) {
                cantidadIngredientePaso = 0;
            }else{
                if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("otro")) {
                    nombreOtroIngredientePaso = dataFragment.getNombre_ingrediente().getText().toString();
                    cantidadIngredientePaso = Integer.parseInt(dataFragment.getCantidad().getText().toString());
                } else {
                    nombreIngredientePaso = dataFragment.getOpcion_ingrediente();
                    esMedible = 1; // es medible
                    cantidadIngredientePaso = Integer.parseInt(dataFragment.getCantidad().getText().toString());
                }


                unidadPaso = dataFragment.getOpcion_medida();
            }
            //agregar el paso a la lista va aca;
            Paso nuevoPaso = new Paso(descripcionPaso, nombreIngredientePaso, cantidadIngredientePaso, unidadPaso, esMedible, nombreOtroIngredientePaso);

            listaPasos.add(nuevoPaso);

            Toast.makeText(v.getContext(), "Se ha agregado un nuevo paso.", Toast.LENGTH_SHORT).show();

            //redibuja la lista
            mSectionsPagerAdapter.notifyDataSetChanged();
            mViewPager.setCurrentItem(listaPasos.size());
        }
    }

    public void verificarCampos(Context context){

        if(dataFragment.getOpcion_ingrediente().equalsIgnoreCase("Ninguno"))
        {
            camposIncompletos = false;

        }else {
            if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("Ingrediente") || dataFragment.getOpcion_medida().equalsIgnoreCase("Medida")) {

                if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("Ingrediente") && dataFragment.getOpcion_medida().equalsIgnoreCase("Medida")) {
                    Toast.makeText(context, "Seleccione un Ingrediente y su Medida", Toast.LENGTH_SHORT).show();
                } else {

                    if (dataFragment.getOpcion_medida().equalsIgnoreCase("Medida")) {
                        Toast.makeText(context, "Seleccione una Medida", Toast.LENGTH_SHORT).show();
                    } else {
                        if (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("Ingrediente")) {
                            Toast.makeText(context, "Seleccione un Ingreddiente", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                camposIncompletos = true;
            }


            if (dataFragment.getCantidad().getText().toString().equals("") || dataFragment.getDescripcion().getText().toString().equals("") ||
                    (dataFragment.getOpcion_ingrediente().equalsIgnoreCase("Otro") && dataFragment.getNombre_ingrediente().getText().toString().equals(""))) {
                camposIncompletos = true;
                Toast.makeText(context, "Algunos de los campos están incompletos", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
