package com.arsoft.cocinapp.events;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.arsoft.cocinapp.modelo.Paso;

import dbhelper.DatabaseHandler;

/**
 * Created by Cristian on 09/10/2016.
 */

public class BotonConfirmModifHandler implements View.OnClickListener{


    Paso paso;
    EditText descripcion, cantidad, nombreOtroIngrediente;
    AppCompatSpinner seleccion_ingrediente, seleccion_medida;
    DatabaseHandler db;
    private boolean camposIncompletos;

    public BotonConfirmModifHandler(EditText descripcion, Paso paso, EditText cantidad, AppCompatSpinner seleccion_ingrediente,AppCompatSpinner seleccion_medida, EditText nombreOtroIngrediente) {
        this.descripcion = descripcion;
        this.paso = paso;
        this.cantidad = cantidad;
        this.seleccion_ingrediente = seleccion_ingrediente;
        this.seleccion_medida = seleccion_medida;
        this.nombreOtroIngrediente = nombreOtroIngrediente;

    }

    @Override
    public void onClick(View v) {

        db = new DatabaseHandler(v.getContext());
        verificarCampos(v.getContext());
        if(!camposIncompletos) {

            if(seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("ninguno")) {
                nombreOtroIngrediente.setText("");
                cantidad.setText("0");
                seleccion_medida.setSelection(3);
                paso.setIdIngrediente(0);
                paso.setMedible(0);
            }else{
                if (seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("otro")) {
                    paso.setIdIngrediente(0);
                    paso.setMedible(0);

                } else {
                    nombreOtroIngrediente.setText("");
                    paso.setMedible(1);
                    paso.setIdIngrediente(seleccion_ingrediente.getSelectedItemPosition());
                }
            }
            ActualizarPaso(paso, descripcion, cantidad, seleccion_ingrediente, seleccion_medida, nombreOtroIngrediente);
            db.modificarPasos(paso);

            Toast.makeText(v.getContext(), "Se guardaron correctamente los cambios", Toast.LENGTH_SHORT).show();
            Activity host = (Activity) v.getContext();
            host.finish();
        }
    }

    public void verificarCampos(Context context){

        if(seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("Ninguno")){
                camposIncompletos = false;
        }else {
            if (seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("Ingrediente") || seleccion_medida.getSelectedItem().toString().equalsIgnoreCase("Medida")) {

                if (seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("Ingrediente") && seleccion_medida.getSelectedItem().toString().equalsIgnoreCase("Medida")) {
                    Toast.makeText(context, "Seleccione un Ingrediente y su Medida", Toast.LENGTH_SHORT).show();
                } else {

                    if (seleccion_medida.getSelectedItem().toString().equalsIgnoreCase("Medida")) {
                        Toast.makeText(context, "Seleccione una Medida", Toast.LENGTH_SHORT).show();
                    } else {
                        if (seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("Ingrediente")) {
                            Toast.makeText(context, "Seleccione un Ingreddiente", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                camposIncompletos = true;
            }


            if (cantidad.getText().toString().equals("") || descripcion.getText().toString().equals("") ||
                    (seleccion_ingrediente.getSelectedItem().toString().equalsIgnoreCase("Otro") && nombreOtroIngrediente.getText().toString().equals(""))) {
                camposIncompletos = true;
                Toast.makeText(context, "Algunos de los campos están incompletos", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void ActualizarPaso(Paso paso, EditText descripcion, EditText cantidad, AppCompatSpinner seleccion_ingrediente, AppCompatSpinner seleccion_medida, EditText nombreOtroIngrediente) {

        paso.setNombreIngrediente(seleccion_ingrediente.getSelectedItem().toString());
        paso.setDescripcion(descripcion.getText().toString());
        paso.setCantidadIngrediente(Integer.parseInt(cantidad.getText().toString()));
        paso.setUnidad(seleccion_medida.getSelectedItem().toString());
        paso.setNombreOtroIngrediente(nombreOtroIngrediente.getText().toString());
        paso.setId(paso.getId());

    }


}
