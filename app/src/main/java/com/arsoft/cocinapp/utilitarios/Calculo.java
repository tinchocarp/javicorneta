package com.arsoft.cocinapp.utilitarios;

import com.arsoft.cocinapp.modelo.Recipiente;

/**
 * Created by ndivito on 15/08/2016.
 */
public class Calculo {

    private static final double pi = 3.141592653589793;

    public static double obtenerMedidas(double al, double radioChico) {
        return calcularVolumenRegular(al, radioChico);
    }

    public static double obtenerMedidas(double al, double radioChico, double radioGrande) {
        return calcularVolumenIrregular(al, radioChico, radioGrande);
    }

    private static double calcularVolumenRegular(double al, double rad) {
        return (pi * rad * rad * al);
    }

    private static double calcularVolumenIrregular(double al, double radC, double radG) {
        return ((pi / 3) * (al * ((radG * radG) + (radC * radC) + (radG * radC))));
    }

    public static double alturaEnRecipiente(double cantidadIngrediente, double radioChico) {
        return (cantidadIngrediente / ( pi * radioChico * radioChico));

    }

    public static double porcentaje(double valorParcial, double valorTotal) {
        return (valorParcial * 100) / valorTotal;
    }

    public static double alturaEnRecipiente(double cantidadIngrediente,double radioChico, double radioGrande) {
       return ((cantidadIngrediente * 3) / (pi * (radioChico * radioChico + radioGrande * radioGrande + radioChico * radioGrande)));

    }

    public static  double alturaEnRecipiente(Recipiente recipiente, double volumenBuscado){
        double pi = 3.14159265359;
        double alturaMinina = 0;
        double alturaTotal = recipiente.getAltura();
        double radioGrande = recipiente.getRadioGrande();
        double radioChico = recipiente.getRadioChico();
        double alturaMaxima = recipiente.getAltura();
        double alturaActual = (alturaMaxima + alturaMinina) / 2;

        double radioGrandeActual = ((alturaActual / alturaTotal) * (radioGrande - radioChico)) + radioChico;

        double volumenActual = (pi / 3) * (alturaActual) * (Math.pow(radioGrandeActual, 2) + Math.pow(radioChico, 2) + radioChico*radioGrandeActual);

            while ((volumenBuscado - 0.1) > volumenActual || (volumenBuscado + 0.1) < volumenActual) {
                if (volumenActual > volumenBuscado) {
                    alturaMaxima = alturaActual;
                }
                else{
                    alturaMinina = alturaActual;
                }
                alturaActual = (alturaMaxima + alturaMinina) / 2;

                radioGrandeActual = ((alturaActual / alturaTotal) * (radioGrande - radioChico)) + radioChico;
                volumenActual = (pi / 3) * (alturaActual) * (Math.pow(radioGrandeActual, 2) + Math.pow(radioChico, 2) + radioChico*radioGrandeActual);
            }

        return alturaActual;
    }

    public static double volumenEnAltura(Recipiente recipiente, int procentaje){
        double pi = 3.14159265359;
        double alturaTotal = recipiente.getAltura();
        double radioGrande = recipiente.getRadioGrande();
        double radioChico = recipiente.getRadioChico();
        double alturaActual = (alturaTotal * (procentaje / (double)100));

        double radioGrandeActual = ((alturaActual / alturaTotal) * (radioGrande - radioChico)) + radioChico;

        double volumenActual = (pi / 3) * (alturaActual) * (Math.pow(radioGrandeActual, 2) + Math.pow(radioChico, 2) + radioChico*radioGrandeActual);

        return volumenActual;
    }

    public static int cantidadRecipientesCompletos(double volumenIngrediente, double volumenRecipiente){
        return (int) (volumenIngrediente / volumenRecipiente);
    }

    public static double calcularCantidadIngregdiente(double volumenRecipiente, double cantidad, double densidad){
        double cantidadEnAgua = cantidad / densidad;
        return cantidadEnAgua % volumenRecipiente;
    }
}
