package com.arsoft.cocinapp.utilitarios;

import com.arsoft.cocinapp.modelo.IngredientePOJO;
import com.arsoft.cocinapp.modelo.Paso;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Porron on 6/8/16.
 */
public class CalculoIngredientes {

    public static String crearListaIngredientes(List<Paso> listaIngredientes){

        StringBuilder text = new StringBuilder();

        Map<String, IngredientePOJO> ingredientesUnicos = new HashMap<>();

        for (Paso paso:listaIngredientes) {

            String nombreIngrediente = paso.getNombreIngrediente();
            Integer antiguaCantidad = paso.getCantidadIngrediente();

            if(ingredientesUnicos.containsKey(nombreIngrediente)){

                int nuevaCantidad = paso.getCantidadIngrediente() + antiguaCantidad;
                ingredientesUnicos.put(nombreIngrediente,new IngredientePOJO(nombreIngrediente,nuevaCantidad,paso.getUnidad()));

            }else {
                ingredientesUnicos.put(nombreIngrediente,new IngredientePOJO(nombreIngrediente,antiguaCantidad,paso.getUnidad()));
            }

        }

        Iterator it = ingredientesUnicos.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            IngredientePOJO ingrediente = (IngredientePOJO) pair.getValue();
            text.append("Lista de Ingredientes: \n");
            text.append(String.format("-%d %s %s",ingrediente.getCantidad(),ingrediente.getUnidad(),ingrediente.getNombre()));
            text.append(System.getProperty("line.separator"));
            it.remove(); // avoids a ConcurrentModificationException
        }

        return text.toString();
    }
}
